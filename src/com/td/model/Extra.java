package com.td.model;

public class Extra {
	private String privacy;
	private String app_version;
	private String about_us;
	
	public String getPrivacy() {
		return privacy;
	}
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}
	
	public String getApp_version() {
		return app_version;
	}
	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}
	
	public String getAbout_us() {
		return about_us;
	}
	public void setAbout_us(String about_us) {
		this.about_us = about_us;
	}
	
}