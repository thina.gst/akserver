package com.td.model;

public class Setting {
	private String user_id;
	private int push_status;
	private int email_status;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public int getPush_status() {
		return push_status;
	}
	public void setPush_status(int push_status) {
		this.push_status = push_status;
	}
	
	public int getEmail_status() {
		return email_status;
	}
	public void setEmail_status(int email_status) {
		this.email_status = email_status;
	}
	
	
}