package com.td.model;
//import java.sql.Blob;
import java.io.File;
//import java.io.DataInputStream;

public class Material {
	

	
	public Material(){
		
	}
	
	public Material(int id, String name, String year, String fname, File file) {
		super();
		this.courseid = id;
		this.coursename = name;
		this.year = year;
		this.filename = fname;
		this.file= file;
	}

	public int getId() {
		return courseid;
	}

	public void setId(int id) {
		this.courseid = id;
	}

	public String getName() {
		return coursename;
	}

	public void setName(String name) {
		this.coursename = name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getFileName() {
		return filename;
	}

	public void setFileName(String fname) {
		this.filename = fname;
	}

	public File getFile() {		
		return file;
	}
	
	public void setFile(File f) {
		this.file=f;
	}
	
	private int courseid;
	private String coursename;
	private String year;
	private String filename;
	private File file;
	
}
