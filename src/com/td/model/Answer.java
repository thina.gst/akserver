package com.td.model;
import java.util.*;
import java.io.File;
import java.sql.Blob;

@SuppressWarnings("unused")
public class Answer {
	private String answer_id;
	private File answer_file;
	private String user_id;
	private String datetime;
	private String question_id;
	private int voice_status;
	
	public String getAnswer_id() {
		return answer_id;
	}
	public void setAnswer_id(String answer_id) {
		this.answer_id = answer_id;
	}
	
	public File getAnswer_file() {
		return answer_file;
	}
	public void setAnswer_file(File answer_file) {
		this.answer_file = answer_file;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	
	public int getVoice_Status() {
		return voice_status;
	}
	public void setVoice_Status(int voice_status) {
		this.voice_status = voice_status;
	}
	
	public String getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}

}