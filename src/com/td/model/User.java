package com.td.model;
import java.util.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
@SuppressWarnings("unused")
public class User {
	private int id;
	private String name;
	private String email;
	private int status;
	private String dept;
	private File image;
	private String password;
	private String in_datetime;
	private String out_datetime;
	private List<String> subject_list = new ArrayList<String>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
	public File getImage() {
		return image;
	}
	public void setImage(File image) {
		this.image = image;
	}
		
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getIn_datetime() {
		return in_datetime;
	}
	public void setIn_datetime(String in_datetime) {
		this.in_datetime = in_datetime;
	}
	
	public String getOut_datetime() {
		return out_datetime;
	}
	public void setOut_datetime(String out_datetime) {
		this.out_datetime = out_datetime;
	}
	
	public List<String> getSubject_list() {
		return subject_list;
	}
	public void setSubject_list(List<String> l) {
		Collections.copy(this.subject_list, l);
	}
	
}
