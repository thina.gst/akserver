package com.td.model;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class Timetable {
	private String user_id;
	private String a,b,c,d,e,f,g,h,i;
	
	public String getUser_Id() {
		return user_id;
	}
	public void setUser_Id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a=a;
	}
	public String getB() {
		return b;
	}
	public void setB(String B) {
		this.b=B;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c=c;
	}
	public String getD() {
		return d;
	}
	public void setD(String D) {
		this.d=D;
	}
	public String getE() {
		return e;
	}
	public void setE(String E) {
		this.e=E;
	}
	public String getF() {
		return f;
	}
	public void setF(String F) {
		this.f=F;
	}
	public String getG() {
		return g;
	}
	public void setG(String G) {
		this.g=G;
	}
	public String getH() {
		return h;
	}
	public void setH(String H) {
		this.h=H;
	}
	public String getI() {
		return i;
	}
	public void setI(String I) {
		this.i=I;
	}
	public void setSubject_list(String a,String b,String c,String d,String e,String f,String g,String h,String i) {
		this.a=a;this.b=b;this.c=c;this.d=d;this.e=e;this.f=f;this.g=g;this.h=h;this.i=i;
	}
}