package com.td.model;

public class Log {
	private String user_id;
	private String in_datetime;
	private String out_datetime;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getIn_datetime() {
		return in_datetime;
	}
	public void setIn_datetime(String in_datetime) {
		this.in_datetime = in_datetime;
	}
	public String getOut_datetime() {
		return out_datetime;
	}
	public void setOut_datetime(String out_datetime) {
		this.out_datetime = out_datetime;
	}
	
}
