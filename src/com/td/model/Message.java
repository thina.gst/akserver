package com.td.model;

public class Message {
	
	
	public Message(int i, String string) {
		this.des = string;
		this.status = i;
	}
	
	public Message() {
		// TODO Auto-generated constructor stub
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	private int status;
	private String des;

}
