package com.td.model;
import java.util.*;
import java.io.File;
import java.sql.Blob;

@SuppressWarnings("unused")
public class Question {
	private String question_id;
	private File question_file;
	private String user_id;
	private String datetime;
	private String answer_id;
	private int voice_status;
	private String sub_id;
	private String exam_id;
	
	public String getQuestion_id() {
		return question_id;
	}
	public void setQuestion_id(String question_id) {
		this.question_id = question_id;
	}
	
	public File getQuestion_file() {
		return question_file;
	}
	public void setQuestion_file(File question_file) {
		this.question_file = question_file;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	public String getAnswer_id() {
		return answer_id;
	}
	public void setAnswer_id(String answer_id) {
		this.answer_id = answer_id;
	}
	
	public int getVoice_Status() {
		return voice_status;
	}
	public void setVoice_Status(int voice_status) {
		this.voice_status = voice_status;
	}
	
	public String getSub_id() {
		return sub_id;
	}
	public void setSub_id(String sub_id) {
		this.sub_id = sub_id;
	}
	
	public String getExam_id() {
		return exam_id;
	}
	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}
}