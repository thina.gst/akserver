package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.FAQ;
import com.td.service.FAQService;

@SuppressWarnings("unused")
@RestController
public class FAQController {

	FAQService service = new FAQService();
	
	@RequestMapping(value = "/updateFAQ", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public String updateFAQ(@PathVariable("id")@RequestBody FAQ pro) {
		return service.updateFAQ(pro);
	}

	@RequestMapping(value = "/getAllSetting/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<FAQ> getAllFAQ(@PathVariable("id") String id) {
		return service.getAllFAQ(id);
	}

}
