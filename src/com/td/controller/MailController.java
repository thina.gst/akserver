package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.td.model.Mail;
import com.td.model.Message;
import com.td.service.MailService;

@SuppressWarnings("unused")
@RestController
public class MailController {

	MailService service = new MailService();

	@RequestMapping(value = "/sendMail", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createStudent(@RequestBody Mail pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.sendMail(pro));
		return message;
	}
}
