package com.td.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.service.ExtraService;

@SuppressWarnings("unused")
@RestController
public class ExtraController {

	ExtraService service = new ExtraService();
	
	@RequestMapping(value = "/getPrivacy", method = RequestMethod.PUT, produces = "application/json")
	public String getPrivacy() {
		return service.getPrivacy();
	}

	@RequestMapping(value = "/setPrivacy/{id}", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json", consumes = "application/json")
	public String setPrivacy(@PathVariable("id") String id) {
		return service.setPrivacy(id);
	}

}
