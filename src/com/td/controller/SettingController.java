package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Setting;
import com.td.model.User;
import com.td.service.SettingService;

@SuppressWarnings("unused")
@RestController
public class SettingController {

	SettingService service = new SettingService();
	
	@RequestMapping(value = "/updateSetting", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public String updateUser(@RequestBody Setting pro) {
		return service.updateSetting(pro);
	}

	@RequestMapping(value = "/getAllSetting", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Setting> getAllSetting() {
		return service.getAllSetting();
	}
	
	@RequestMapping(value = "/getUserSetting/{id}", method = RequestMethod.GET, headers = "Accept=application/json",produces = "application/json", consumes = "application/json")
	public Setting getUserSetting(@PathVariable("id")  String id) {
		return service.getUserSetting(id);
	}

}
