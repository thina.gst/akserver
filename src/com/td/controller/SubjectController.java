package com.td.controller;

import java.util.ArrayList;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Subject;
import com.td.service.SubjectService;

@SuppressWarnings("unused")
@RestController
public class SubjectController {

	SubjectService service = new SubjectService();
	
	@RequestMapping(value = "/addsubject", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createSubject(@RequestBody Subject pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.addSubject(pro));
		return message;
	}

	@RequestMapping(value = "/getSubjectbyid/{id}", produces = "application/json", method = RequestMethod.GET)
	public Subject getSubjectbyid(@PathVariable("id") String id) {
		Subject pro = service.getSubjectbyid(id);
		return pro;
	}

	@RequestMapping(value = "/deleteSubjectbyid/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteSubjectbyid(@PathVariable("id") String id) {
		service.deleteSubjectbyid(id);
	}
	
	@RequestMapping(value = "/getAllSubject", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Subject> getAllSubject() {
		System.out.println("PRINTING :: " + service.getAllSubject());
		return service.getAllSubject();//"TEST RES";
	}
}
