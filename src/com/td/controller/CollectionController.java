package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Collection;
import com.td.service.CollectionService;

@SuppressWarnings("unused")
@RestController
public class CollectionController {

	CollectionService service = new CollectionService();
	
	@RequestMapping(value = "/updateCollection", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public String updateCollection(@RequestBody Collection pro) {
		return service.updateCollection(pro);
	}

	@RequestMapping(value = "/getSubjectsbyCollectionid", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json", consumes = "application/json")
	public String[] getSubjectsbyCollectionid(@RequestBody Collection pro) {
		return service.getSubjectsbyCollectionid(pro);
	}

}
