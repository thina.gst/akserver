package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Timetable;
import com.td.service.TimetableService;

@SuppressWarnings("unused")
@RestController
public class TimetableController {

	TimetableService service = new TimetableService();
	
	@RequestMapping(value = "/addTimetable/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createUser(@PathVariable("id") String id,@RequestBody Timetable pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.addTimetable(pro));
		return message;
	}

	@RequestMapping(value = "/userTimetable/{id}", produces = "application/json", method = RequestMethod.GET)
	public Timetable getTimetablebyid(@PathVariable("id") String id) {
		Timetable pro = service.getTimetablebyid(id);
		return pro;
	}

}
