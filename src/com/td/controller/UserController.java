package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.User;
import com.td.service.UserService;

@RestController
public class UserController {

	UserService service = new UserService();

	@RequestMapping(value = "/allusers", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<User> getAllUsers() {
		return service.getAllUsers();
	}
	@RequestMapping(value = "/saveuser", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createUser(@RequestBody User pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.addUser(pro));
		return message;
	}

	@RequestMapping(value = "/user/{id}", produces = "application/json", method = RequestMethod.GET)
	public User getUserbyid(@PathVariable("id") String id) {
		User pro = service.getUserbyid(id);
		return pro;
	}

	@RequestMapping(value = "/updateuser", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public int updateUser(@RequestBody User pro) {
		return service.updateUser(pro);
	}

	@RequestMapping(value = "/deleteuser/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteUserByID(@PathVariable("id") String id) {
		service.deleteUserByID(id);
	}
	
	@RequestMapping(value = "/blockUser/{id}", method = RequestMethod.PUT, produces = "application/json",consumes = "application/json")
	public void blockUser(@PathVariable("id") String id) {
		service.blockUser(id);
	}
	
	@RequestMapping(value = "/unblockUser/{id}", method = RequestMethod.PUT, produces = "application/json",consumes = "application/json")
	public void unblockUser(@PathVariable("id") String id) {
		service.unblockUser(id);
	}

}
