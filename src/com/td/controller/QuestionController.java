package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.td.model.Message;
import com.td.model.Question;
import com.td.model.User;
import com.td.service.QuestionService;

@SuppressWarnings("unused")
@RestController
public class QuestionController {

	QuestionService service = new QuestionService();

	@RequestMapping(value = "/allquestions", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Question> viewAllQuestion() {
		return service.viewAllQuestion();
	}
	
	@RequestMapping(value = "/savequestion", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createStudent(@RequestBody Question pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.insertQuestion(pro));
		return message;
	}

	@RequestMapping(value = "/question/{id}", produces = "application/json", method = RequestMethod.GET)
	public Question getQuestionByID(@PathVariable("id") String id) {
		Question pro = service.getQuestionByID(id);
		return pro;
	}

	@RequestMapping(value = "/updatequestion/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public int updateProduct(@PathVariable("id") String id,
			@RequestBody Question pro) {
		return service.updateQuestion(id, pro);
	}

	@RequestMapping(value = "/deletequestion/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public void deleteStudent(@PathVariable("id") String id) {
		service.deleteQuestionByID(id);
	}
	
	@RequestMapping(value = "/getAllQuestionBySubjectID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Question> getAllQuestionBySubjectID(@PathVariable("id") String id) {
		return service.getAllQuestionBySubjectID(id);
	}
	
	@RequestMapping(value = "/getAllQuestionByUserID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Question> getAllQuestionByUserID(@PathVariable("id") String id) {
		return service.getAllQuestionByUserID(id);
	}
	
	@RequestMapping(value = "/getAllQuestionByExamID/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Question> getAllQuestionByExamID(@PathVariable("id") String id) {
		return service.getAllQuestionByExamID(id);
	}

}
