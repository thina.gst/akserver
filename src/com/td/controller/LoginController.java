package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.td.model.Login;
import com.td.model.Message;
import com.td.service.LoginService;

@SuppressWarnings("unused")
@RestController

public class LoginController {
	
	LoginService loginService = new LoginService();
	
	@RequestMapping(value = "/newLogin", method = RequestMethod.POST, headers = "Accept=application/json", 
            produces="application/json", consumes="application/json")
	 public Message login(@RequestBody Login login){
		Message message = new Message();
		System.out.println("LoginController :: Login");
		if(loginService.validateCredentials(login)){
			if(loginService.authoriseUser(login)){
				message.setDes("Login Success");
				message.setStatus(1);
			}else{
				message.setDes("Login Failure");
				message.setStatus(0);
			}
		}else{
			message.setDes("Invalid Data");
			message.setStatus(0);
		}
		return message;
	 }

}
