package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Log;
import com.td.service.LogService;

@SuppressWarnings("unused")
@RestController
public class LogController {
	LogService service = new LogService();
	
	@RequestMapping(value = "/getUserlog/{id}", produces = "application/json", method = RequestMethod.GET)
	public Log getUserbyid(@PathVariable("id") String id) {
		Log pro = service.getUserbyid(id);
		return pro;
	}

	@RequestMapping(value = "/updatelog", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public int updateUserlog(@RequestBody Log pro) {
		return service.updateUserlog(pro);
	}
}
