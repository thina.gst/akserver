package com.td.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.td.model.Answer;
import com.td.model.Message;
import com.td.model.User;
import com.td.service.AnswerService;

@SuppressWarnings("unused")
@RestController
public class AnswerController {

	AnswerService service = new AnswerService();

	@RequestMapping(value = "/allanswers", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Answer> viewAllAnswer() {
		return service.viewAllAnswer();
	}
	/*
	@RequestMapping(value = "/allUsers", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<User> getAllUsers() {
		return service.getAllUsers();
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message addUser(@RequestBody User user) {
		Message message = new Message(0, "An Error Occured");
		if(service.insertUser(user)=="User Added"){
			message.setStatus(1);
			message.setDes("User added Successfully");
		}
		return message;
	}
*/
	@RequestMapping(value = "/saveanswer", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Message createStudent(@RequestBody Answer pro) {
		Message message = new Message();
		message.setStatus(1);
		message.setDes(service.insertAnswer(pro));
		return message;
	}

	@RequestMapping(value = "/answer/{id}", produces = "application/json", method = RequestMethod.GET)
	public Answer getAnswerByID(@PathVariable("id") String id) {
		Answer ans = service.getAnswerByID(id);
		return ans;
	}

	@RequestMapping(value = "/updateanswer/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public Message updateAnswer(@PathVariable("id") String id,
			@RequestBody Answer pro) {
		pro.setAnswer_id(id);
		return service.updateAnswer(id, pro);
	}

	@RequestMapping(value = "/deleteanswer/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public Message deleteAnswerByID(@PathVariable("id") String id) {
		return service.deleteAnswerByID(id);
	}

}
