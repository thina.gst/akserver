package com.td.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.td.model.Message;
import com.td.model.Examination;
import com.td.service.ExaminationService;

@SuppressWarnings("unused")
@RestController
public class ExaminationController {

	ExaminationService service = new ExaminationService();
	
	@RequestMapping(value = "/updateExamination", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public String updateExamination(@RequestBody Examination pro) {
		return service.updateExamination(pro);
	}

	@RequestMapping(value = "/getExaminationbyid/{id}", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json", consumes = "application/json")
	public Examination getExaminationbyid(@PathVariable("id") String id) {
		return service.getExaminationbyid(id);
	}
	
	@RequestMapping(value = "/getAllExamination", method = RequestMethod.GET, headers = "Accept=application/json", produces = "application/json", consumes = "application/json")
	public ArrayList<Examination> getAllExamination() {
		return service.getAllExamination();
	}

}
