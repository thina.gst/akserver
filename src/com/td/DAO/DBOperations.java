package com.td.DAO;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
//
import com.td.model.Answer;
import com.td.model.Collection;
import com.td.model.Examination;
import com.td.model.FAQ;
import com.td.model.Log;
import com.td.model.Login;
import com.td.model.Mail;
import com.td.model.Message;
import com.td.model.Question;
import com.td.model.Setting;
import com.td.model.Subject;
import com.td.model.Timetable;
import com.td.model.User;
import com.td.util.DBConnection;
@SuppressWarnings("unused")
public class DBOperations implements DAOInterface {
	Connection con = DBConnection.getConnection().newConnection();
	int result = 0;
	int result1 = 0,result2 = 0,result3=0,result4 = 0;
	
	@Override
	public Message insertQuestion(Question obj) {
		String query = "INSERT INTO question VALUES (?, ?, ?, ?, ?,?,?,?)";
		Message message = new Message();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, obj.getQuestion_id());

			File f = obj.getQuestion_file();
			FileInputStream input = new FileInputStream(f);
			preparedStatement.setBinaryStream(2, input);
			
			preparedStatement.setString(3, obj.getUser_id());
			preparedStatement.setString(4, obj.getDatetime());
			preparedStatement.setString(5, obj.getAnswer_id());
			preparedStatement.setInt(6, obj.getVoice_Status());
			preparedStatement.setString(7, obj.getSub_id());
			preparedStatement.setString(8, obj.getExam_id());
			
			result = preparedStatement.executeUpdate();
			message.setDes("Question Added Successfully");
			message.setStatus(result);
			return message;
		} catch (SQLException | FileNotFoundException e) {
			result = 0;
			message.setDes(e.getMessage().toString());
			message.setStatus(result);
			return message;
			
		}

	}
	
	@Override
	public com.td.model.Message insertAnswer(Answer obj) {
		String query = "INSERT INTO answer VALUES (?, ?, ?, ?, ?, ?)";
		com.td.model.Message message = new com.td.model.Message();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, obj.getAnswer_id());

			File f = obj.getAnswer_file();
			FileInputStream input = new FileInputStream(f);
			preparedStatement.setBinaryStream(2, input);
			
			preparedStatement.setString(3, obj.getUser_id());
			preparedStatement.setString(4, obj.getDatetime());
			preparedStatement.setString(5, obj.getQuestion_id());
			preparedStatement.setInt(6, obj.getVoice_Status());
			
			result = preparedStatement.executeUpdate();
			
			message.setDes("Answer Saved Successfully");
			message.setStatus(1);
			
			return message;
		} catch (SQLException | FileNotFoundException e) {
			System.out.println("Error:: " + e.getMessage());
			message.setDes(e.getMessage());
			message.setStatus(0);
			return message;
			
		}

	}

	@Override
	public ArrayList<Question> viewAllQuestion() {
		String query = "select * from question";
		ArrayList<Question> questionList = new ArrayList<Question>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Question obj = new Question();
				obj.setQuestion_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = new File("/Users/gsthina/Desktop/team_td/products/answerKey/test-data/tempQues.docx");
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setQuestion_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setAnswer_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				obj.setSub_id(res.getString(7));
				obj.setExam_id(res.getString(8));
				
				questionList.add(obj);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			Question obj = new Question();
			obj.setQuestion_id("No Material Available");
			questionList.add(obj);
		}
		return questionList;

	}
	
	@Override
	public ArrayList<Answer> viewAllAnswer() {
		String query = "select * from answer";
		ArrayList<Answer> answerList = new ArrayList<Answer>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			int i = 0;
			while (res.next()) {
				Answer obj = new Answer();
				obj.setAnswer_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = new File("/Users/gsthina/Desktop/team_td/products/answerKey/test-data/temp_"+ (i++) +".docx");
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setAnswer_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setQuestion_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				answerList.add(obj);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			Answer obj = new Answer();
			obj.setAnswer_id("No Material Available");
			answerList.add(obj);
		}
		return answerList;

	}
	

	@SuppressWarnings("resource")
	@Override
	public Question getQuestionByID (String id) {
		try {
		String query = "select * from question where question_id='" + id + "'";
		Question obj = new Question();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				obj.setQuestion_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setQuestion_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setAnswer_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				obj.setSub_id(res.getString(7));
				obj.setExam_id(res.getString(8));
			}
			return obj;
		} catch (SQLException | IOException e) {
			Question obj = new Question();
			e.printStackTrace();
			obj.setQuestion_id("No Material Available");
			return obj;
		}
	}
	
	@Override
	public ArrayList<Question> getAllQuestionBySubjectID(String id) {
		String query = "select * from question WHERE sub_id='"+id+"'";
		ArrayList<Question> questionList = new ArrayList<Question>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Question obj = new Question();
				obj.setQuestion_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setQuestion_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setAnswer_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				obj.setSub_id(res.getString(7));
				obj.setExam_id(res.getString(8));
				
				questionList.add(obj);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			Question obj = new Question();
			obj.setQuestion_id("No Material Available");
			questionList.add(obj);
		}
		return questionList;

	}
	
	@Override
	public ArrayList<Question> getAllQuestionByUserID(String id) {
		String query = "select * from question WHERE user_id='"+id+"'";
		ArrayList<Question> questionList = new ArrayList<Question>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Question obj = new Question();
				obj.setQuestion_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setQuestion_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setAnswer_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				obj.setSub_id(res.getString(7));
				obj.setExam_id(res.getString(8));
				
				questionList.add(obj);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			Question obj = new Question();
			obj.setQuestion_id("No Material Available");
			questionList.add(obj);
		}
		return questionList;

	}
	
	
	@Override
	public ArrayList<Question> getAllQuestionByExamID(String id) {
		String query = "select * from question WHERE exam_id='"+id+"'";
		ArrayList<Question> questionList = new ArrayList<Question>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Question obj = new Question();
				obj.setQuestion_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setQuestion_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setAnswer_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
				obj.setSub_id(res.getString(7));
				obj.setExam_id(res.getString(8));
				
				questionList.add(obj);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			Question obj = new Question();
			obj.setQuestion_id("No Material Available");
			questionList.add(obj);
		}
		return questionList;

	}
	
	
	@Override
	public Answer getAnswerByID (String id) {
		try {
		String query = "select * from answer where question_id='" + id + "'";
		Answer obj = new Answer();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				obj.setAnswer_id(res.getString(1));
				Blob blob=res.getBlob(2);
				//---FILE WORK---
				File f = new File("/Users/gsthina/Desktop/team_td/products/answerKey/test-data/temp.docx");
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				obj.setAnswer_file(f);
				//---FILE WORK OVER---
				obj.setUser_id(res.getString(3));
				obj.setDatetime(res.getString(4));
				obj.setQuestion_id(res.getString(5));
				obj.setVoice_Status(res.getInt(6));
			}
			return obj;
		} catch (SQLException | IOException e) {
			Answer obj = new Answer();
			e.printStackTrace();
			obj.setAnswer_id("No Material Available");
			return obj;
		}
	}

	@Override
	public int updateQuestion(String id, Question obj) {
		try {
		    String query = "UPDATE question SET question_id=?, question_file=?, user_id=?, date_time=?, answer_id= ?, voice_status= ?, sub_id= ? ,exam_id=? WHERE  COURSE_ID='" + id + "'";
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, obj.getQuestion_id());

			File f = obj.getQuestion_file();
			FileInputStream input = new FileInputStream(f);
			preparedStatement.setBinaryStream(2, input);
			
			preparedStatement.setString(3, obj.getUser_id());
			preparedStatement.setString(4, obj.getDatetime());
			preparedStatement.setString(5, obj.getAnswer_id());
			preparedStatement.setInt(6, obj.getVoice_Status());
			preparedStatement.setString(7, obj.getSub_id());
			preparedStatement.setString(8, obj.getExam_id());
			
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException | FileNotFoundException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public Message updateAnswer(String id, Answer obj) {
		Message message = new Message();
		try {
		    //String query = "UPDATE answer SET answer_id=?, answer_file=?, user_id=?, date_time=?, question_id= ?, voice_status= ? WHERE  question_id='" + id + "';UPDATE question SET answer_id='"+obj.getAnswer_id()+"' WHERE question_id='"+id+"'";
			String query = "UPDATE answer SET answer_file=?, user_id=?, date_time=?, voice_status= ? WHERE answer_id='" + id + "'";
			PreparedStatement preparedStatement = con.prepareStatement(query);

			File f = obj.getAnswer_file();
			FileInputStream input = new FileInputStream(f);
			preparedStatement.setBinaryStream(1, input);
			preparedStatement.setString(2, obj.getUser_id());
			preparedStatement.setString(3, obj.getDatetime());
			preparedStatement.setInt(4, obj.getVoice_Status());
			
			result = preparedStatement.executeUpdate();
			message.setStatus(result);
			message.setDes("Answer Updated Successfully");
			return message;
		} catch (SQLException | FileNotFoundException e) {
			message.setStatus(result);
			message.setDes(e.getMessage().toString());
			return message;
		}
	}
	

	@Override
	public int deleteQuestionByID(String id) {
		String query = "DELETE FROM question WHERE question_id='" + id + "'";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException e) {
			result=0;
			e.printStackTrace();
			return(result);
		}
	}
	
	@Override
	public Message deleteAnswerByID(String id) {
		String query = "DELETE FROM answer WHERE answer_id='" + id + "'";
		Message message = new Message();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			message.setDes("Answer Deleted Successfully");
			message.setStatus(result);
			return message;
		} catch (SQLException e) {
			result=0;
			message.setDes(e.getMessage());
			message.setStatus(result);
			return message;
		}
	}

	@Override
	public boolean loginUser(Login login) {
		try {
		String query = "SELECT ID from user_login where id='"+ login.getUsername() + "' AND password='"+ login.getPassword() + "' AND status='0' ";
		
			PreparedStatement preparedStatement = con.prepareStatement(query);
			
			ResultSet resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				System.out.println(resultSet.getInt(1));
				return true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return false;
	}

	@Override
	public ArrayList<User> getAllUsers() {
		String query = "select id,email,password,status,dept,name,image from user_login";
		ArrayList<User> userList = new ArrayList<User>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				User user = new User();
				user.setId(res.getInt(1));
				user.setEmail(res.getString(2));
				user.setPassword(res.getString(3));
				user.setStatus(res.getInt(4));
				user.setDept(res.getString(5));
				user.setName(res.getString(6));
				Blob blob=res.getBlob(7);
				
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				user.setImage(f);
				//---FILE WORK OVER---
				
				userList.add(user);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			User user = new User();
			user.setName("No User Available");
			userList.add(user);
		}
		return userList;
	}

	@Override
	public int addUser(User user) {
		String query = "INSERT INTO user_login VALUES (?, ?, ?, ?, ?, ?, ?)";
		
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setInt(1, user.getId());
			preparedStatement.setString(2, user.getEmail());
			preparedStatement.setString(3, user.getPassword());
			preparedStatement.setInt(4, user.getStatus());
			preparedStatement.setString(5, user.getDept());
			preparedStatement.setString(6, user.getName());
			
			File f = user.getImage();
			FileInputStream input = new FileInputStream(f);
			
			preparedStatement.setBinaryStream(7, input);
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException | FileNotFoundException e) {
			result = 0;
			return(result);
		}
	}
	
	@Override
	public int deleteUserByID(String id) {
		
		String query = "DELETE FROM user_login WHERE id ="+ id +";"+"DELETE FROM user_log WHERE user_id =" + id +";" + "DELETE FROM timetable WHERE user_id =" + id + ";"+"DELETE FROM setting WHERE user_id =" + id + ";"+"DELETE FROM collection WHERE user_id ='" + id + "'";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException e) {
			result=0;
			e.printStackTrace();
			return(result);
		}
	}
	
	@Override
	public int updateUser(User user) {
		try {
		    String query = "UPDATE user_login SET email=?, password=?, status=?, dept= ?,name=?,image=? WHERE  id='" +user.getId()+ "'";
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, user.getEmail());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setInt(3, user.getStatus());
			preparedStatement.setString(4, user.getDept());
			preparedStatement.setString(5, user.getName());
			
			File f = user.getImage();
			FileInputStream input = new FileInputStream(f);
			preparedStatement.setBinaryStream(6, input);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException | FileNotFoundException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public int blockUser(String id) {
		try {
		    String query = "UPDATE user_login SET status='0' WHERE id='" +id+ "'"; //0 -block 1-unblock
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public int unblockUser(String id) {
		try {
		    String query = "UPDATE user_login SET status='1' WHERE id='" +id+ "'"; //1-unblock
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public User getUserbyid(String id) {
		String query = "select id,email,password,status,dept,name,image from user_login where id='" + id + "'";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
				User user = new User();
				user.setId(res.getInt(1));
				user.setEmail(res.getString(2));
				user.setPassword(res.getString(3));
				user.setStatus(res.getInt(4));
				user.setDept(res.getString(5));
				user.setName(res.getString(6));
				Blob blob=res.getBlob(7);
				
				//---FILE WORK---
				File f = null;
				InputStream in = blob.getBinaryStream();
				@SuppressWarnings("resource")
				FileOutputStream out = new FileOutputStream(f);
				byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
				int len = 0;
				while ((len = in.read(buff)) != -1) {
				    out.write(buff, 0, len);
				}
				user.setImage(f);
				//---FILE WORK OVER---
				return (user);
		} catch (SQLException | IOException e) {
			e.printStackTrace();
			User user = new User();
			user.setName("No User Available");
			return(user);
		}
	}
	
	@Override
	public int updateUserlog(Log user) {
		try {
		    String query = "INSERT INTO user_log (id, in_datetime, out_datetime) VALUES(" +user.getUser_id()+ "," +user.getOut_datetime()+ "," +user.getIn_datetime() +") ON DUPLICATE KEY UPDATE in_datetime = " +user.getOut_datetime()+ " ,out_datetime=" +user.getIn_datetime();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public Log getUserlogbyid(String id) {
		try {
		    String query = "select * from user_log where user_id="+id;
		    PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			Log user = new Log();
			user.setIn_datetime(res.getString(1));
			user.setOut_datetime(res.getString(2));
			return(user);
		} catch (SQLException e) {
			Log user=new Log();
			e.printStackTrace();
			user.setIn_datetime("no log available");
			return(user);
		}
	}
	
	
	@Override
	public int addTimetable(Timetable t) {
		String query = "INSERT INTO timetable VALUES (?, ?,?, ?,?, ?,?, ?,?) ON DUPLICATE KEY UPDATE a=?,b= ?,c=?,d= ?,e=?,f= ?,g=?,h= ?,i=? ";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1,t.getUser_Id());
			preparedStatement.setString(2,t.getA());
			preparedStatement.setString(3,t.getB());preparedStatement.setString(4,t.getC());preparedStatement.setString(5,t.getD());preparedStatement.setString(6,t.getE());
			preparedStatement.setString(7,t.getF());preparedStatement.setString(8,t.getG());preparedStatement.setString(9,t.getH());preparedStatement.setString(10,t.getI());
			preparedStatement.setString(11,t.getA());
			preparedStatement.setString(12,t.getB());preparedStatement.setString(13,t.getC());preparedStatement.setString(14,t.getD());preparedStatement.setString(15,t.getE());
			preparedStatement.setString(16,t.getF());preparedStatement.setString(17,t.getG());preparedStatement.setString(18,t.getH());preparedStatement.setString(19,t.getI());
			
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException e) {
			result = 0;
			return(result);
		}
	}
	
	@Override
	public Timetable getTimetablebyid(String id) {
		try {
		    String query = "select * from timetable where user_id= "+id;
		    PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			Timetable user = new Timetable();
			user.setUser_Id(res.getString(1));
			user.setA(res.getString(2));
			user.setB(res.getString(3));
			user.setC(res.getString(4));
			user.setD(res.getString(5));
			user.setE(res.getString(6));
			user.setF(res.getString(7));
			user.setG(res.getString(8));
			user.setH(res.getString(9));
			user.setI(res.getString(10));
			return(user);
		} catch (SQLException e) {
			Timetable user=new Timetable();
			e.printStackTrace();
			user.setUser_Id("no log available");
			return(user);
		}
	}
	
	@Override
	public int addSubject(Subject obj) {
		String query = "INSERT INTO subject VALUES (?, ?) ";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1,obj.getSubject_id());
			preparedStatement.setString(2,obj.getSubject_name());
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException e) {
			result = 0;
			return(result);
		}
	}
	
	@Override
	public Subject getSubjectbyid(String id) {
		String query = "Select * FROM subject where id="+id+"'";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			Subject obj = new Subject();
			obj.setSubject_id(res.getString(1));
			obj.setSubject_name(res.getString(2));
			return(obj);
		} catch (SQLException e) {
			Subject obj = new Subject();
			obj.setSubject_name("NO SUBJECT AVAILABLE");
			return(obj);
		}
	}
	
	@Override
	public List<Subject> getAllSubject(){
		String query = "select * from subject";
		List<Subject> subList = new ArrayList<Subject>();
		System.out.println("ALL SUBJECTS DBO CALLED");
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				System.out.println("INTO WHILE");
				Subject obj = new Subject();
				System.out.println(res.getString(1));
				obj.setSubject_id(res.getString(1));
				obj.setSubject_name(res.getString(2));
				System.out.println(obj.getSubject_name());
				subList.add(obj);
				System.out.println(subList);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Subject obj = new Subject();
			obj.setSubject_id("No subjects available");
			subList.add(obj);
		}
		return subList;
	}
	
	@Override
	public int deleteSubjectbyid(String id) {
		String query = "DELETE FROM subject WHERE id ="+ id +";"+"DELETE FROM question WHERE sub_id =" + id;
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(result);
		} catch (SQLException e) {
			result=0;
			e.printStackTrace();
			return(result);
		}
	}
	
	@Override
	public int updateSetting(Setting user) {
		try {
		    String query = "INSERT INTO setting (user_id, push_status, email_status) VALUES(" +user.getUser_id()+ "," +user.getPush_status()+ "," +user.getEmail_status() +") ON DUPLICATE KEY UPDATE push_status = " +user.getPush_status()+ " ,email_status=" +user.getEmail_status();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public ArrayList<Setting> getAllSetting(){
		String query = "select user_id,push_status,email_status from setting";
		ArrayList<Setting> userList = new ArrayList<Setting>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Setting user = new Setting();
				user.setUser_id(res.getString(1));
				user.setPush_status(res.getInt(2));
				user.setEmail_status(res.getInt(3));
				
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Setting user = new Setting();
			user.setUser_id("No User Available");
			userList.add(user);
		}
		return userList;
	}
	
	@Override
	public Setting getUserSetting(String id){
		String query = "select user_id,push_status,email_status from setting WHERE user_id='"+id+"'";
		try {
			Setting user = new Setting();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				user.setUser_id(res.getString(1));
				user.setPush_status(res.getInt(2));
				user.setEmail_status(res.getInt(3));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			Setting user = new Setting();
			user.setUser_id("No User Available");
			return user;
		}
	}
	
	@Override
	public int updateFAQ(FAQ user) {
		try {
		    String query = "INSERT INTO faq (question, answer) VALUES("+user.getQuestion()+ "," +user.getAnswer() +") ON DUPLICATE KEY UPDATE question = " +user.getQuestion()+ " ,answer=" +user.getAnswer();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public ArrayList<FAQ> getAllFAQ(String num){
		String query = "select * from faq LIMIT "+num ;
		ArrayList<FAQ> userList = new ArrayList<FAQ>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				FAQ user = new FAQ();
				user.setQuery_id(res.getInt(1));
				user.setQuestion(res.getString(2));
				user.setAnswer(res.getString(3));
				
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			FAQ user = new FAQ();
			user.setQuery_id(0);
			userList.add(user);
		}
		return userList;
	}
	
	@Override
	public int updateExamination(Examination user) {
		try {
		    String query = "INSERT INTO examination (exam_id, exam_name) VALUES("+user.getExam_id()+ "," +user.getExam_name() +") ON DUPLICATE KEY UPDATE exam_id = " +user.getExam_id()+ " ,exam_name=" +user.getExam_name();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public Examination getExaminationbyid(String id) {
		String query = "Select * FROM examination where exam_id= "+id+"'";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			Examination obj = new Examination();
			
			obj.setExam_id(res.getString(1));
			obj.setExam_name(res.getString(2));
			
			return(obj);
		} catch (SQLException e) {
			Examination obj = new Examination();
			obj.setExam_id("NO SUBJECT AVAILABLE");
			return(obj);
		}
	}
	
	@Override
	public ArrayList<Examination> getAllExamination(){
		String query = "select * from examination ";
		ArrayList<Examination> examList = new ArrayList<Examination>();
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			ResultSet res = preparedStatement.executeQuery();
			while (res.next()) {
				Examination exam = new Examination();
				exam.setExam_id(res.getString(1));
				exam.setExam_id(res.getString(2));
				examList.add(exam);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			Examination exam = new Examination();
			exam.setExam_id("No exam available");
			examList.add(exam);
		}
		return examList;
	}
	
	@Override
	public int updateCollection(Collection user) {
		try {
		    String query = "INSERT INTO collection (user_id,coll_name,question_id) VALUES("+user.getUser_id()+ "," +user.getColl_name() +"," +user.getQuestion_id() +") ON DUPLICATE KEY UPDATE exam_id = " +user.getColl_name()+ " ,exam_name=" +user.getQuestion_id();
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			return(1);
		} catch (SQLException e) {
			e.printStackTrace();
			return(0);
		}
	}
	
	@Override
	public String[] getSubjectsbyCollectionid(Collection obj) {
		String query = "Select * FROM collection where user_id= '?' AND coll_id='?' ";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1,obj.getUser_id());
			preparedStatement.setString(2,obj.getColl_name());
			result = preparedStatement.executeUpdate();
			ResultSet res = preparedStatement.executeQuery();
			String x=res.getString(1);
			return(x.split(","));
		} catch (SQLException e) {
			String x="No subjects available";
			return(x.split(","));
		}
	}
	
	@Override
	public String getPrivacy() {
		String query = "Select value FROM extra where purpose= 'privacy' ";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			ResultSet res = preparedStatement.executeQuery();
			String x=res.getString(1);
			return(x);
		} catch (SQLException e) {
			String x="No privacy details available";
			return(x);
		}
		
	}
	
	@Override
	public String setPrivacy(String id) {
		String query = "UPDATE extra SET value='"+id+"' where purpose= 'privacy' ";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(query);
			result = preparedStatement.executeUpdate();
			if(result==1)
				return("privacy updated");
			else
				return("privacy not updated");
		} catch (SQLException e) {
			String x="Error while updating privacy value";
			return(x);
		}
		
	}

	@Override
	public String sendMail(Mail obj) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	@Override
//	public String sendMail(Mail obj) {
//		try { 
//		final String from=obj.getFrom();
//		final String password=obj.getPassword();
//		String to=obj.getTo();
//		String subject=obj.getSubject();
//		String messsage=obj.getMessage();
//		File x=obj.getFile();
//		
//		//Get properties object    
//        Properties props = new Properties();    
//        props.put("mail.smtp.host", "smtp.gmail.com");    
//        props.put("mail.smtp.socketFactory.port", "465");    
//        props.put("mail.smtp.socketFactory.class",    
//                  "javax.net.ssl.SSLSocketFactory");    
//        props.put("mail.smtp.auth", "true");    
//        props.put("mail.smtp.port", "465");    
//        //get Session   
//        Session session = Session.getDefaultInstance(props,    
//         new javax.mail.Authenticator() {    
//         protected PasswordAuthentication getPasswordAuthentication() {    
//         return new PasswordAuthentication(from,password);  
//         }    
//        });    
//        //compose message 
//           
//      	  String msgText1 = messsage;
//      	  MimeMessage msg = new MimeMessage(session);
//            msg.setFrom(new InternetAddress(from));
//            InternetAddress[] address = {new InternetAddress(to)};
//            msg.setRecipients(Message.RecipientType.TO, address);
//            msg.setSubject(subject);
//       
//            // create and fill the first message part
//            MimeBodyPart mbp1 = new MimeBodyPart();
//            mbp1.setText(msgText1);
//       
//            // create the second message part
//            MimeBodyPart mbp2 = new MimeBodyPart();
//         // attach the file to the message
//            FileDataSource fds = new FileDataSource(x);
//            mbp2.setDataHandler(new DataHandler(fds));
//            mbp2.setFileName(fds.getName());
//       
//            // create the Multi part and add its parts to it
//            Multipart mp = new MimeMultipart();
//            mp.addBodyPart(mbp1);
//            mp.addBodyPart(mbp2);
//       
//            // add the Multi part to the message
//            msg.setContent(mp);
//       
//            // set the Date: header
//            msg.setSentDate(new Date());
//             
//            // send the message
//            Transport.send(msg);   
//         return("message sent successfully");    
//        } catch (MessagingException e) {
//        	return("message not sent,Try again");
//        	}
//	}
	
	
}
