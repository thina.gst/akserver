package com.td.DAO;

import java.util.ArrayList;
import java.util.List;

import com.td.model.Answer;
import com.td.model.Collection;
import com.td.model.Examination;
import com.td.model.FAQ;
import com.td.model.Log;
import com.td.model.Login;
import com.td.model.Mail;
import com.td.model.Material;
import com.td.model.Message;
import com.td.model.Question;
import com.td.model.Setting;
import com.td.model.Subject;
import com.td.model.Timetable;
import com.td.model.User;

@SuppressWarnings("unused")
public interface DAOInterface {
	public Message insertQuestion(Question obj);
	public Message insertAnswer(Answer obj);
	
	public ArrayList<Question> viewAllQuestion();
	public ArrayList<Answer> viewAllAnswer();
	
	public Question getQuestionByID (String id);
	public ArrayList<Question> getAllQuestionBySubjectID (String id);
	public ArrayList<Question> getAllQuestionByUserID(String id);
	public ArrayList<Question> getAllQuestionByExamID(String id);
	
	public Answer getAnswerByID (String id);
	
	public int updateQuestion(String id, Question mat);
	public Message updateAnswer(String id, Answer obj);
	//--------------------------------------------
	public int deleteQuestionByID(String id);
	public Message deleteAnswerByID(String id);
	
	public boolean loginUser(Login login);
	
	public ArrayList<User> getAllUsers();
	public int addUser(User user);
	public int deleteUserByID(String id);
	public int updateUser(User user);
	public User getUserbyid(String id);
	public int blockUser(String id); //0 -block 
	public int unblockUser(String id); // 1-unblock
	
	public int updateUserlog(Log user); //update and insert both
	public Log getUserlogbyid(String id);
	
	public int addTimetable(Timetable t); //update and insert both
	public Timetable getTimetablebyid(String id);
	
	public int addSubject(Subject obj);
	public Subject getSubjectbyid(String id);
	public List<Subject> getAllSubject();
	public int deleteSubjectbyid(String id);
	
	public int updateSetting(Setting user); //update and insert both
	public Setting getUserSetting(String id);
	public ArrayList<Setting> getAllSetting();
	
	public int updateFAQ(FAQ user); //update and insert both
	public ArrayList<FAQ> getAllFAQ(String num);  // limit value
	
	public int updateExamination(Examination user); //update and insert both
	public Examination getExaminationbyid(String id);
	public ArrayList<Examination> getAllExamination();
	
	public int updateCollection(Collection user); //update and insert both
	public String[] getSubjectsbyCollectionid(Collection obj);
	
	public String sendMail(Mail obj);
	
	public String getPrivacy();
	public String setPrivacy(String id);
}
