package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Question;
import com.td.model.User;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class QuestionService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String insertQuestion(Question obj) {
		return new DBOperations().insertQuestion(obj).getDes().toString();
	}

	

	public ArrayList<Question> viewAllQuestion() {
		return new DBOperations().viewAllQuestion();
	}
	
	public ArrayList<Question> getAllQuestionBySubjectID(String id) {
		return new DBOperations().getAllQuestionBySubjectID(id);
	}
	
	public ArrayList<Question> getAllQuestionByUserID(String id) {
		return new DBOperations().getAllQuestionByUserID(id);
	}
	
	public ArrayList<Question> getAllQuestionByExamID(String id) {
		return new DBOperations().getAllQuestionByUserID(id);
	}

	public Question getQuestionByID(String id) {

		return new DBOperations().getQuestionByID(id);
	}

	public int updateQuestion(String id, Question obj) {
		obj.setQuestion_id(new HashData().hash(obj.getQuestion_id()));
		obj.setUser_id(new HashData().hash(obj.getUser_id()));
		obj.setDatetime(new HashData().hash(obj.getDatetime()));
		obj.setAnswer_id(new HashData().hash(obj.getAnswer_id()));
		obj.setExam_id(new HashData().hash(obj.getExam_id()));
		obj.setSub_id(new HashData().hash(obj.getSub_id()));
		//obj.setVoice_Status(new HashData().hash(obj.getVoice_Status()));
		//obj.setQuestion_file(new HashData().hash(obj.getQuestion_file()));
		if (new DBOperations().updateQuestion(id, obj) == 1) {
			return (1);
		} else {
			return (0);
		}
	}

	public void deleteQuestionByID(String id) {
		new DBOperations().deleteQuestionByID(id);

	}

}
