package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.User;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class UserService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String addUser(User obj) {
		//obj.setId(new HashData().hash(obj.getId()));
		//obj.setImage(new HashData().hash(obj.getImage()));
		obj.setName(new HashData().hash(obj.getName()));
		obj.setEmail(new HashData().hash(obj.getEmail()));
		//obj.setStatus(new HashData().hash(obj.getStatus()));
		obj.setDept(new HashData().hash(obj.getDept()));
		obj.setPassword(new HashData().hash(obj.getPassword()));
		obj.setDept(new HashData().hash(obj.getDept()));
		if (new DBOperations().addUser(obj) == 1) {
			return "User Added";
		} else {
			return "Error. User Not Added";
		}

	}

	

	public ArrayList<User> getAllUsers() {
		return new DBOperations().getAllUsers();
	}

	public User getUserbyid(String id) {
		return new DBOperations().getUserbyid(id);
	}

	public int updateUser(User obj) {
		//obj.setId(new HashData().hash(obj.getId()));
		//obj.setImage(new HashData().hash(obj.getImage()));
		obj.setName(new HashData().hash(obj.getName()));
		obj.setEmail(new HashData().hash(obj.getEmail()));
		//obj.setStatus(new HashData().hash(obj.getStatus()));
		obj.setDept(new HashData().hash(obj.getDept()));
		obj.setPassword(new HashData().hash(obj.getPassword()));
		obj.setDept(new HashData().hash(obj.getDept()));
		if (new DBOperations().updateUser(obj) == 1) {
			return (1);
		} else {
			return (0);
		}
	}

	public void deleteUserByID(String id) {
		new DBOperations().deleteUserByID(id);

	}
	
	public void blockUser(String id) {
		new DBOperations().blockUser(id);
	}
	public void unblockUser(String id) {
		new DBOperations().blockUser(id);
	}

}
