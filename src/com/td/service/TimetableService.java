package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Timetable;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class TimetableService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String addTimetable(Timetable obj) {
		obj.setUser_Id(new HashData().hash(obj.getUser_Id()));
		obj.setA(new HashData().hash(obj.getA()));
		obj.setB(new HashData().hash(obj.getB()));
		obj.setC(new HashData().hash(obj.getC()));
		obj.setD(new HashData().hash(obj.getD()));
		obj.setE(new HashData().hash(obj.getE()));
		obj.setF(new HashData().hash(obj.getF()));
		obj.setG(new HashData().hash(obj.getG()));
		obj.setH(new HashData().hash(obj.getH()));
		obj.setI(new HashData().hash(obj.getI()));
		if (new DBOperations().addTimetable(obj) == 1) {
			return "Timetable Added";
		} else {
			return "Error. Timetable Not Added";
		}

	}

	public Timetable getTimetablebyid(String id) {
		return new DBOperations().getTimetablebyid(id);
	}
	
}
