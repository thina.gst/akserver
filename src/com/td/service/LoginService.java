package com.td.service;

import java.io.Serializable;

import com.td.DAO.DBOperations;
import com.td.model.Login;

public class LoginService implements Serializable {
	private static final long serialVersionUID = 1L;

	public boolean validateCredentials(Login login) {
		System.out.println("LoginService :: Validate");
		// simple validate if needed.
		boolean result = false;
		if(!login.getUsername().equals("")&&!login.getPassword().equals("")){
			result = true;
		}
		return result;
	}

	public boolean authoriseUser(Login login) {
		System.out.println("LoginService :: Authorise");
//		login.setPassword(new HashData().hash(login.getPassword()));
		if (new DBOperations().loginUser(login)) {
			return true;
		} else {
			return false;
		}
	}

}
