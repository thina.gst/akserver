package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Mail;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class MailService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String sendMail(Mail obj) {
		return(new DBOperations().sendMail(obj));
	}

}
