package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Setting;
import com.td.model.User;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class SettingService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String updateSetting(Setting obj) {
		obj.setUser_id(new HashData().hash(obj.getUser_id()));
		//obj.setPush_status(new HashData().hash(obj.getPush_status()));
		//obj.setEmail_status(new HashData().hash(obj.getEmail_status()));

		if (new DBOperations().updateSetting(obj) == 1) {
			return "Setting Updated";
		} else {
			return "Error.Setting Not Updated";
		}

	}

	public ArrayList<Setting> getAllSetting() {
		return new DBOperations().getAllSetting();
	}
	
	public Setting getUserSetting(String id) {
		return new DBOperations().getUserSetting(id);
	}

}
