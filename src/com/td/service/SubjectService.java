package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Subject;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class SubjectService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String addSubject(Subject obj) {
		obj.setSubject_id(new HashData().hash(obj.getSubject_id()));
		obj.setSubject_name(new HashData().hash(obj.getSubject_name()));

		if (new DBOperations().addSubject(obj) == 1) {
			return "Subject Added";
		} else {
			return "Error. Subject Not Added";
		}

	}

	public Subject getSubjectbyid(String id) {
		return new DBOperations().getSubjectbyid(id);
	}

	public void deleteSubjectbyid(String id) {
		new DBOperations().deleteSubjectbyid(id);

	}
	
	public List<Subject> getAllSubject() {
		return new DBOperations().getAllSubject();
	}

}
