package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.FAQ;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class FAQService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String updateFAQ(FAQ obj) {
		//obj.setQuery_id(new HashData().hash(obj.getQuery_id()));
		obj.setQuestion(new HashData().hash(obj.getQuestion()));
		obj.setAnswer(new HashData().hash(obj.getAnswer()));

		if (new DBOperations().updateFAQ(obj) == 1) {
			return "Setting Updated";
		} else {
			return "Error.Setting Not Updated";
		}

	}

	public ArrayList<FAQ> getAllFAQ(String num) {
		return new DBOperations().getAllFAQ(num);
	}

}
