package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Examination;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class ExaminationService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String updateExamination(Examination obj) {
		obj.setExam_id(new HashData().hash(obj.getExam_id()));
		obj.setExam_name(new HashData().hash(obj.getExam_name()));

		if (new DBOperations().updateExamination(obj) == 1) {
			return "Examination Added/Updated";
		} else {
			return "Error.Examination Not Added/Updated";
		}

	}

	public Examination getExaminationbyid(String id) {
		return new DBOperations().getExaminationbyid(id);
	}
	
	public ArrayList<Examination> getAllExamination() {
		return new DBOperations().getAllExamination();
	}

}
