package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class ExtraService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String getPrivacy() {
		return(new DBOperations().getPrivacy());
	}

	public String setPrivacy(String id) {
		return(new DBOperations().setPrivacy(id));
	}


}
