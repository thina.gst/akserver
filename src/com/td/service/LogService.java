package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Log;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class LogService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Log getUserbyid(String id) {
		return new DBOperations().getUserlogbyid(id);
	}

	public int updateUserlog(Log obj) {
		obj.setUser_id(new HashData().hash(obj.getUser_id()));
		obj.setIn_datetime(new HashData().hash(obj.getIn_datetime()));
		obj.setOut_datetime(new HashData().hash(obj.getOut_datetime()));
		
		if (new DBOperations().updateUserlog(obj) == 1) {
			return (1);
		} else {
			return (0);
		}
	}

}
