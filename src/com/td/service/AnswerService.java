package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Answer;
import com.td.model.Message;
import com.td.model.Question;
import com.td.model.User;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class AnswerService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String insertAnswer(Answer obj) {
		Message message = new DBOperations().insertAnswer(obj);
		return message.getDes();
	}

	

	public ArrayList<Answer> viewAllAnswer() {
		return new DBOperations().viewAllAnswer();
	}

	public Answer getAnswerByID(String id) {

		return new DBOperations().getAnswerByID(id);
	}

	public Message updateAnswer(String id, Answer obj) {
		return new DBOperations().updateAnswer(id, obj);
	}

	public Message deleteAnswerByID(String id) {
		return new DBOperations().deleteAnswerByID(id);
	}



	/*public List<User> getAllUsers() {
		return new DBOperations().getAllUsers();
	}



	public String insertUser(User user) {
		if (new DBOperations().addUser(user) == 1) {
			return "User Added";
		} else {
			return "Error Occured";
		}
	}*/

}
