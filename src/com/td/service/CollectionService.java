package com.td.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.td.DAO.DBOperations;
import com.td.model.Collection;
import com.td.util.HashData;
import com.td.util.SHA256;

@SuppressWarnings("unused")
public class CollectionService implements Serializable {

	private static final long serialVersionUID = 1L;

	public String updateCollection(Collection obj) {
		obj.setUser_id(new HashData().hash(obj.getUser_id()));
		obj.setColl_name(new HashData().hash(obj.getColl_name()));
		obj.setQuestion_id(new HashData().hash(obj.getQuestion_id()));
		
		if (new DBOperations().updateCollection(obj) == 1) {
			return "Collection Added/Updated";
		} else {
			return "Error.Collection Not Added/Updated";
		}

	}

	public String[] getSubjectsbyCollectionid(Collection obj){
		obj.setUser_id(new HashData().hash(obj.getUser_id()));
		obj.setColl_name(new HashData().hash(obj.getColl_name()));
		obj.setQuestion_id(new HashData().hash(obj.getQuestion_id()));
		return new DBOperations().getSubjectsbyCollectionid(obj);
	}

}
