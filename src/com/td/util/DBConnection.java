package com.td.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	private static DBConnection dbcon=null;

	private DBConnection() {
	}

	public static DBConnection getConnection() {
		if (dbcon == null)
			dbcon = new DBConnection();

		return dbcon;
	}

	public Connection newConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://139.59.75.172/answerKey","root","GSThina@2025");
			System.out.println(con);
			return con;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}

}
