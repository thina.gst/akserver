package com.td.util;

import com.td.util.SHA256;

public class HashData {
	public String hash(String name) {
		return new SHA256().getSHA256Hash(name);
	}
}
