webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<section id=\"app-header\">\n  <h1>{{title}}</h1>\n</section>\n<nav>\n  <button *ngIf=\"!_location.isCurrentPathEqualTo('/dashboard')\" class=\"btn btn-danger\" (click)=\"backButtonClicked()\">Go Back</button>\n  <a *ngIf=\"!_cacheService.get('current')\" routerLink=\"/login\">\n    <div class=\"btn btn-primary menu right\">Login</div>\n  </a>\n  <a *ngIf=\"_cacheService.get('current')\" routerLink=\"/login\">\n    <div class=\"btn btn-danger menu right\" (click)=\"logout()\">Logout</div>\n  </a>\n  <a routerLink=\"/dashboard\">\n    <div class=\"btn btn-info menu right\">Dashboard</div>\n  </a>\n  <a routerLink=\"/testWS\">\n    <div class=\"btn btn-warning menu right\">Users</div>\n  </a>\n</nav>\n<br/>\n<!--<div class=\"container justify-content-md-center\">\n  <div class=\"row\">\n    <div class=\"col\">\n      <h5>Enter Amount to Transfer:</h5>\n    </div>\n    <div class=\"col\">\n      <input class=\"form-control\" type=\"number\" [(ngModel)]=\"data\" />\n    </div>\n  </div>\n    <div class=\"btn btn-success menu right\" (click)=\"send('thina', data)\">SEND TO THINA</div>\n    <div class=\"btn btn-success menu right\" (click)=\"send('swadhin', data)\">SEND TO SWADHIN</div>\n    <div class=\"btn btn-success menu right\" (click)=\"send('infant', data)\">SEND TO INFANT</div>\n    <div class=\"btn btn-success menu right\" (click)=\"send('cp', data)\">SEND TO CP</div>\n</div>-->\n<router-outlet></router-outlet>\n\n<style>\n\n</style>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppComponent = (function () {
    function AppComponent(_location, _cacheService, router, service) {
        this._location = _location;
        this._cacheService = _cacheService;
        this.router = router;
        this.service = service;
        this.title = 'AngularJS + Spring MVC';
        this._cacheService.useStorage(__WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["b" /* CacheStoragesEnum */].LOCAL_STORAGE);
        this._cacheService.setGlobalPrefix("check4cache_");
        console.log("AppComponent :: ", _cacheService.exists('current'));
    }
    AppComponent.prototype.backButtonClicked = function () {
        console.log(this._location.isCurrentPathEqualTo("/dashboard"));
        if (!this._location.isCurrentPathEqualTo("/dashboard") || !this._location.isCurrentPathEqualTo("/testWS")) {
            this._location.back();
        }
    };
    AppComponent.prototype.send = function (component, amt) {
        console.log(amt);
        this.service.setTransferAmount(component, amt);
        this.router.navigate(['/thina']);
    };
    AppComponent.prototype.logout = function () {
        this._cacheService.removeAll();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'thina',
        template: __webpack_require__("../../../../../src/app/app.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__services_master_service__["a" /* MasterService */]) === "function" && _d || Object])
], AppComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__bill_create_component__ = __webpack_require__("../../../../../src/app/bill.create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ws_component__ = __webpack_require__("../../../../../src/app/ws.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__dashboard_dashboard_service__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__dashboard_swadhin_component__ = __webpack_require__("../../../../../src/app/dashboard/swadhin.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__dashboard_thina_component__ = __webpack_require__("../../../../../src/app/dashboard/thina.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__dashboard_cp_component__ = __webpack_require__("../../../../../src/app/dashboard/cp.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__dashboard_infant_component__ = __webpack_require__("../../../../../src/app/dashboard/infant.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__login_login_component__ = __webpack_require__("../../../../../src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_bill_service__ = __webpack_require__("../../../../../src/app/services/bill.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__dynamic_form_dynamic_form_component__ = __webpack_require__("../../../../../src/app/dynamic-form/dynamic-form.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__dynamic_form_question_dynamic_form_question_component__ = __webpack_require__("../../../../../src/app/dynamic-form-question/dynamic-form-question.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["d" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* RouterModule */].forRoot([
                {
                    path: 'dashboard',
                    component: __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard_component__["a" /* DashboardComponent */]
                }, {
                    path: 'create',
                    component: __WEBPACK_IMPORTED_MODULE_6__bill_create_component__["a" /* BillCreateComponent */]
                }, {
                    path: '',
                    redirectTo: '/login',
                    pathMatch: 'full'
                }, {
                    path: 'testWS',
                    component: __WEBPACK_IMPORTED_MODULE_7__ws_component__["a" /* WsComponent */]
                }, {
                    path: 'thina',
                    component: __WEBPACK_IMPORTED_MODULE_11__dashboard_thina_component__["a" /* Thina */]
                }, {
                    path: 'swadhin',
                    component: __WEBPACK_IMPORTED_MODULE_10__dashboard_swadhin_component__["a" /* Swadin */]
                }, {
                    path: 'cp',
                    component: __WEBPACK_IMPORTED_MODULE_12__dashboard_cp_component__["a" /* Cp */]
                }, {
                    path: 'infant',
                    component: __WEBPACK_IMPORTED_MODULE_13__dashboard_infant_component__["a" /* Infant */]
                }, {
                    path: 'login',
                    component: __WEBPACK_IMPORTED_MODULE_14__login_login_component__["a" /* LoginComponent */]
                }, {
                    path: 'form',
                    component: __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]
                }
            ]),
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */]
        ],
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard_component__["a" /* DashboardComponent */],
            __WEBPACK_IMPORTED_MODULE_6__bill_create_component__["a" /* BillCreateComponent */],
            __WEBPACK_IMPORTED_MODULE_7__ws_component__["a" /* WsComponent */],
            __WEBPACK_IMPORTED_MODULE_10__dashboard_swadhin_component__["a" /* Swadin */],
            __WEBPACK_IMPORTED_MODULE_11__dashboard_thina_component__["a" /* Thina */],
            __WEBPACK_IMPORTED_MODULE_12__dashboard_cp_component__["a" /* Cp */],
            __WEBPACK_IMPORTED_MODULE_13__dashboard_infant_component__["a" /* Infant */],
            __WEBPACK_IMPORTED_MODULE_14__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_18__dynamic_form_dynamic_form_component__["a" /* DynamicFormComponent */],
            __WEBPACK_IMPORTED_MODULE_19__dynamic_form_question_dynamic_form_question_component__["a" /* DynamicFormQuestionComponent */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_9__dashboard_dashboard_service__["a" /* DashboardService */],
            __WEBPACK_IMPORTED_MODULE_17_ng2_cache_ng2_cache__["a" /* CacheService */],
            __WEBPACK_IMPORTED_MODULE_15__services_master_service__["a" /* MasterService */],
            __WEBPACK_IMPORTED_MODULE_16__services_bill_service__["a" /* BillService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/bill.create.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!_cacheService.exists('current')\" class=\"container materialise bill_header\">\r\n    {{loginMessage}}\r\n</div>\r\n<div *ngIf=\"_cacheService.exists('current')\" class=\"container materialise bill_header\">\r\n    <div class=\"row\">\r\n        <button [disabled]=\"!bill.invoiceNumber\" type=\"button\" class=\"btn btn-danger btn-sm margin right\" (click)=\"service.deleteBill(bill.innvoiceNumber)\">\r\n          <span class=\"glyphicon glyphicon-trash\"></span> \r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-default btn-sm margin right\" (click)=\"resetBill()\">\r\n          <span class=\"glyphicon glyphicon-refresh\"></span> \r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-success btn-sm margin right\" (click)=\"saveBill(bill)\">\r\n          <span class=\"glyphicon glyphicon-floppy-saved\"></span> \r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-primary btn-sm margin right\" (click)=\"print(bill)\">\r\n          <span class=\"glyphicon glyphicon-print\"></span> \r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-dark btn-sm margin right\" (click)=\"viewToggle(bill)\">\r\n          <span class=\"glyphicon glyphicon-eye-open\"></span>\r\n        </button>\r\n    </div>\r\n    <div id=\"print-section\">\r\n        <div class=\"row\">\r\n            <div class=\"input inline-block\">\r\n                <input *ngIf=\"!preview\" type=\"text\" [(ngModel)]=\"bill.invoiceNumber\" class=\"form-control\" placeholder=\"Invoice Number\">\r\n                <p *ngIf=\"preview\">Invoice No: <b>{{bill.invoiceNumber}}</b></p>\r\n            </div>\r\n            <!--<div class=\"input-group\">-->\r\n            <div class=\"input right inline-block\">\r\n                <input *ngIf=\"!preview\" type=\"date\" [(ngModel)]=\"bill.dateValue\" class=\"form-control\" placeholder=\"Date\" (change)=\"billOnDateChange(bill.dateValue)\">\r\n                <!--<span class=\"input-group-addon\" (click)=\"getTodayDate()\">Reload</span>-->\r\n                <p *ngIf=\"preview\">Bill Date: <b>{{bill.dateValue}}</b></p>\r\n            </div>\r\n            <!--</div>-->\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"input\"></div>\r\n            <div class=\"input right\">\r\n                <select *ngIf=\"!preview\" type=\"text\" [(ngModel)]=\"bill.billType\" class=\"form-control\" placeholder=\"Bill Type\" (change)=\"billTypeOnChange()\">\r\n                    <option value=\"BT00\" class=\"form-control\">Retailer</option>\r\n                    <option value=\"BT01\" class=\"form-control\">Distributer</option>\r\n                </select>\r\n            <p *ngIf=\"preview&&bill.billType==='BT00'\">Bill Type: <b>Retailer</b></p>\r\n            <p *ngIf=\"preview&&bill.billType==='BT01'\">Bill Type: <b>Distributer</b></p>\r\n            </div>\r\n        </div>\r\n        <!--<div class=\"materialise\"></div>-->\r\n\r\n        <div class=\"row\">\r\n            <table class=\"table\">\r\n                <tr class=\"materialise\" *ngIf=\"!preview\">\r\n                    <td class=\"table-content-center\">+</td>\r\n                    <td><select class=\"form-control\" (change)=\"getProductDetails(newProductItem.desc)\" [(ngModel)]=\"newProductItem.desc\"\r\n                            [disabled]=\"disableToEdit\">\r\n                            <option *ngFor=\"let item of productItems\">{{item.desc}}</option>\r\n                        </select></td>\r\n                    <td class=\"table-content-center\"><input class=\"form-control\" type=\"text\" [(ngModel)]=\"newProductItem.hsnCode\" [disabled]=\"disableToEdit\"/></td>\r\n                    <td class=\"table-content-center\"><input class=\"form-control\" type=\"number\" min=\"0\" [(ngModel)]=\"newProductItem.quantity\" /></td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT00'\">Rs. {{newProductItem.retRate.toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT01'\">Rs. {{newProductItem.distRate.toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT00'\">Rs. {{(newProductItem.retRate*newProductItem.quantity).toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT01'\">Rs. {{(newProductItem.distRate*newProductItem.quantity).toFixed(2)}}/-</td>\r\n                    <td>\r\n                        <button type=\"button\" class=\"btn btn-success inline-block\" (click)=\"addNewProductToList(newProductItem)\">{{updateText}}</button>\r\n                    </td>\r\n                </tr>\r\n\r\n                <tr class=\"materialise-padd thead\" style=\"background-color: grey; color: white\">\r\n                    <td class=\"table-content-center min-width\">S.I</td>\r\n                    <td class=\"huge-width\">Description</td>\r\n                    <td class=\"table-content-center\">HSN Code</td>\r\n                    <td class=\"table-content-center\">Quantity</td>\r\n                    <td class=\"table-content-center\">Rate</td>\r\n                    <td class=\"table-content-center\">Price</td>\r\n                    <td *ngIf=\"!preview\">Action</td>\r\n                </tr>\r\n                <tr *ngFor=\"let item of bill.billItems; let i = index;\" class=\"materialise-padd margin-1\">\r\n                    <td class=\"table-content-center\">{{i+1}}</td>\r\n                    <td>{{item.desc}}</td>\r\n                    <td class=\"table-content-center\">{{item.hsnCode}}</td>\r\n                    <td class=\"table-content-center\">{{item.quantity}}</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT00'\">Rs. {{item.retRate.toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT01'\">Rs. {{item.distRate.toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT00'\">Rs. {{(item.retRate * item.quantity).toFixed(2)}}/-</td>\r\n                    <td class=\"table-content-right\" *ngIf=\"bill.billType=='BT01'\">Rs. {{(item.distRate * item.quantity).toFixed(2)}}/-</td>\r\n                    <td *ngIf=\"!preview\" class=\"inline-block row\" >\r\n                        <button type=\"button\" class=\"btn btn-info inline-block btn-sm\" (click)=\"editBillItem(i)\">\r\n                        <span class=\"glyphicon glyphicon-pencil btn-icon\"></span>\r\n                    </button>\r\n                        <button type=\"button\" class=\"btn btn-danger inline-block btn-sm\" (click)=\"removeBillItem(i)\">\r\n                        <span class=\"glyphicon glyphicon-remove btn-icon\"></span>\r\n                    </button>\r\n                    </td>\r\n                </tr>\r\n            </table>\r\n            <div *ngIf=\"!bill.billItems.length\" class=\"alert alert-danger\" role=\"alert\">\r\n                Please add products...\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"bill.billItems.length\" class=\"medium-width right\">\r\n            <table class=\"table\">\r\n                <tr class=\"\">\r\n                    <td>Price:</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">Rs. {{bill.price.toFixed(2)}}/-</td>\r\n                </tr>\r\n                <tr class=\"\">\r\n                    <td>CGST</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">{{config.cgst}} %</td>\r\n                </tr>\r\n                <tr class=\"\">\r\n                    <td>CGST</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">{{config.sgst}} %</td>\r\n                </tr>\r\n                <tr class=\"\">\r\n                    <td>GST Price</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">Rs. {{bill.gstPrice.toFixed(2)}}/-</td>\r\n                </tr>\r\n                <tr class=\"\">\r\n                    <td>Round Off Amount</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">Rs. {{bill.roundOffAmount.toFixed(2)}}/-</td>\r\n                </tr>\r\n                <tr class=\"h1\">\r\n                    <td>Total Price</td>\r\n                    <td></td>\r\n                    <td class=\"table-content-right\">Rs. {{bill.totalPrice.toFixed(2)}}/-</td>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/bill.create.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BillCreateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_bill_service__ = __webpack_require__("../../../../../src/app/services/bill.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BillCreateComponent = (function () {
    function BillCreateComponent(_cacheService, service) {
        this._cacheService = _cacheService;
        this.service = service;
        this.disableToEdit = false;
        this.updateText = "ADD";
        this.newProductItem = {
            desc: "",
            hsnCode: "",
            quantity: 0,
            distRate: 0,
            retRate: 0
        };
        // public billItems: any = [];
        this.config = {
            cgst: 9,
            sgst: 9
        };
        this.bill = {
            invoiceNumber: '',
            billItems: [],
            price: 0,
            roundOffAmount: 0,
            gstPrice: 0,
            totalPrice: 0,
            billType: 'BT00',
            dateValue: '',
        };
        this.productItems = [
            {
                desc: "Bislery 1 Litre",
                hsnCode: "1011H04",
                quantity: 40,
                distRate: 24.5,
                retRate: 26
            },
            {
                desc: "Bislery 5 Litre",
                hsnCode: "1011H04",
                quantity: 20,
                distRate: 120.0,
                retRate: 125
            },
            {
                desc: "Limca 0.5 Litre",
                hsnCode: "1011H04",
                quantity: 45,
                distRate: 15.0,
                retRate: 18
            },
            {
                desc: "Bislery Soda",
                hsnCode: "1011H04",
                quantity: 25,
                distRate: 20.0,
                retRate: 22
            }
        ];
        this.preview = false;
        console.log("BillCreateComponent :: ", _cacheService.exists('bill'));
    }
    BillCreateComponent.prototype.ngOnInit = function () {
        this.getTodayDate();
        // for(var i=0; i<this.productItems.length; ++i){
        //     this.bill.billItems.push(this.productItems[i]); // for dev
        // }
        // Check for CacheService
        if (this._cacheService.exists('bill')) {
            this.bill = this._cacheService.get('bill');
        }
        this.updatePrice();
        // this.bill.billItems = this.productItems; // for development
    };
    BillCreateComponent.prototype.billTypeOnChange = function () {
        this.updatePrice();
    };
    BillCreateComponent.prototype.billOnDateChange = function (date) {
        console.log(date);
        this.updatePrice();
    };
    BillCreateComponent.prototype.getTodayDate = function () {
        var today = new Date();
        this.bill.dateValue = (today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()).toString();
        console.log(this.bill.dateValue);
    };
    BillCreateComponent.prototype.getProductDetails = function (product) {
        console.log(product);
        var self = this;
        // Write Code to fetch Product
        this.productItems.forEach(function (item) {
            if (item.desc === product) {
                self.newProductItem = {
                    desc: item.desc,
                    hsnCode: item.hsnCode,
                    distRate: item.distRate,
                    retRate: item.retRate,
                    quantity: 0
                };
            }
        });
    };
    BillCreateComponent.prototype.addNewProductToList = function (product) {
        console.log("Adding New Product :: ", product);
        this.bill.billItems.push(product);
        this.resetNewProduct();
        console.log(this.bill.billItems);
        this.disableToEdit = false;
        this.updateText = "ADD MORE";
        this.updatePrice();
    };
    BillCreateComponent.prototype.editBillItem = function (index) {
        this.disableToEdit = true;
        this.updateText = "UPDATE";
        if (this.newProductItem.desc != "") {
            this.bill.billItems.push(this.newProductItem);
            this.newProductItem = this.bill.billItems[index];
            if (index !== -1) {
                this.bill.billItems.splice(index, 1);
            }
        }
        else {
            this.newProductItem = this.bill.billItems[index];
            if (index !== -1) {
                this.bill.billItems.splice(index, 1);
            }
        }
        this.updatePrice();
        console.log(this.bill.billItems);
    };
    BillCreateComponent.prototype.removeBillItem = function (index) {
        if (index !== -1) {
            this.bill.billItems.splice(index, 1);
        }
        console.log(this.bill.billItems);
        this.updatePrice();
    };
    BillCreateComponent.prototype.resetNewProduct = function () {
        this.newProductItem = {
            desc: "",
            hsnCode: "",
            distRate: 0,
            retRate: 0,
            quantity: 0
        };
    };
    BillCreateComponent.prototype.updatePrice = function () {
        this.bill.price = 0;
        this.bill.roundOffAmount = 0;
        this.bill.gstPrice = 0;
        this.bill.totalPrice = 0;
        // Price Calc
        for (var i = 0; i < this.bill.billItems.length; ++i) {
            this.bill.billItems[i].price = (this.bill.billType == 'BT00') ? this.bill.billItems[i].retRate : (this.bill.billType == 'BT01') ? this.bill.billItems[i].distRate : 0;
            this.bill.price += (this.bill.billItems[i].price * this.bill.billItems[i].quantity);
        }
        // GST Price
        this.bill.gstPrice = (Math.round(this.bill.price * ((this.config.cgst + this.config.sgst) / 100) * 100) / 100);
        // Round Off Price
        // if(Math.round(this.bill.gstPrice)>this.bill.gstPrice){
        this.bill.roundOffAmount = Math.round(this.bill.gstPrice) - this.bill.gstPrice;
        // }else{
        //     this.bill.roundOffAmount = this.bill.gstPrice - Math.round(this.bill.gstPrice);
        // }
        // Total Price
        this.bill.totalPrice = (this.bill.price + Math.round(this.bill.gstPrice));
        this.updateCache();
    };
    BillCreateComponent.prototype.updateCache = function () {
        var currentBill = {
            invoiceNumber: this.bill.invoiceNumber,
            date: this.bill.dateValue,
            billType: this.bill.billType,
            billItems: this.bill.billItems,
            dateValue: this.bill.dateValue
        };
        this._cacheService.set('bill', currentBill);
    };
    BillCreateComponent.prototype.resetBill = function () {
        this.bill = {
            invoiceNumber: '',
            billItems: [],
            price: 0,
            roundOffAmount: 0,
            gstPrice: 0,
            totalPrice: 0,
            billType: 'BT00',
            dateValue: ''
        };
        this.updateCache();
    };
    BillCreateComponent.prototype.saveBill = function (bill) {
        this.service.saveBill(bill);
        this.resetBill();
    };
    BillCreateComponent.prototype.print = function (bill) {
        this.preview = true;
        var self = this;
        setTimeout(function () {
            var content = document.getElementById('print-section').innerHTML;
            self.service.print(content, bill);
        }, 1000);
    };
    BillCreateComponent.prototype.viewToggle = function (bill) {
        this.preview = (this.preview) ? false : true;
        this.service.viewBill(bill);
    };
    return BillCreateComponent;
}());
BillCreateComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'bill-create',
        template: __webpack_require__("../../../../../src/app/bill.create.component.html")
        // templateUrl: 'dashboard.component.html'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_bill_service__["a" /* BillService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_bill_service__["a" /* BillService */]) === "function" && _b || Object])
], BillCreateComponent);

var _a, _b;
//# sourceMappingURL=bill.create.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/cp.component.html":
/***/ (function(module, exports) {

module.exports = "<a routerLink=\"/thina\">\r\n    <div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">\r\n        CP\r\n    </div>\r\n</a>\r\n<div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">Sent Amount: {{this.service.getTransferAmount('cp')}}</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard/cp.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_service__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Cp = (function () {
    // public balance:any = this.service.getTransferAmount('cp');
    function Cp(dashboardService, service) {
        this.dashboardService = dashboardService;
        this.service = service;
    }
    Cp.prototype.ngOnInit = function () {
        this.dashboardService.getStudents();
        console.log("Cp Component :: ", this.service.getTransferAmount('cp'));
    };
    return Cp;
}());
Cp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'cp-dash',
        template: __webpack_require__("../../../../../src/app/dashboard/cp.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */]) === "function" && _b || Object])
], Cp);

var _a, _b;
//# sourceMappingURL=cp.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"container\" style=\"text-align: center\">\r\n    <h2 *ngIf=\"_cacheService.exists('current')\">Welcome <b>{{ _cacheService.get('current') }}</b></h2>\r\n    <h2 *ngIf=\"!_cacheService.exists('current')\">Log in to continue..</h2>\r\n    <div *ngIf=\"!_cacheService.exists('current')\">\r\n        \r\n    </div>\r\n    <!--<div *ngIf=\"_cacheService.exists('current')\">\r\n        <div class=\"swadhin inline right\">\r\n            <swadhin-dash></swadhin-dash>\r\n        </div>\r\n        <div class=\"thina inline col-xs-12\">\r\n            <thina-dash></thina-dash>\r\n        </div>\r\n        <div class=\"cp inline right col-xs-12\">\r\n            <cp-dash></cp-dash>\r\n        </div>\r\n        <div class=\"infant inline col-xs-12\">\r\n            <infant-dash></infant-dash>\r\n        </div>\r\n    </div>-->\r\n</section>\r\n\r\n<style>\r\n\r\n</style>"

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = (function () {
    function DashboardComponent(_location, _cacheService) {
        this._location = _location;
        this._cacheService = _cacheService;
        this.Users = [];
        this._cacheService.useStorage(__WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["b" /* CacheStoragesEnum */].LOCAL_STORAGE);
        console.log("DashboardComponent :: ", _cacheService.exists('current'));
    }
    DashboardComponent.prototype.backButtonClicked = function () {
        this._location.back();
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'add-member',
        //   template: `
        //   <h3>Add Member</h3>
        //   <input type="text" [(ngModel)]="username" />
        //   <button (click)="showData(username)">View Name</button>
        //   `
        template: __webpack_require__("../../../../../src/app/dashboard/dashboard.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common__["f" /* Location */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _b || Object])
], DashboardComponent);

var _a, _b;
//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/dashboard.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardService = (function () {
    function DashboardService(http, _location) {
        this.http = http;
        this._location = _location;
        this.result = [{
                name: "",
                age: 0
            }];
        this.message = "Please Wait...";
        this.students = [
            { age: 11, name: 'Mr. Nice' },
            { age: 12, name: 'Narco' },
            { age: 13, name: 'Bombasto' },
            { age: 14, name: 'Celeritas' },
            { age: 15, name: 'Magneta' },
            { age: 16, name: 'RubberMan' },
            { age: 17, name: 'Dynama' },
            { age: 18, name: 'Dr IQ' },
            { age: 19, name: 'Magma' },
            { age: 20, name: 'Tornado' }
        ];
    }
    DashboardService.prototype.getStudents = function () {
        var _this = this;
        try {
            this.http.get("http://10.44.72.211:9080/SpringWsAngular/acs/ws/students").subscribe(function (data) {
                // this.result = this.saturateData(data);//.then(saturatedData => {
                _this.result = data;
                // console.log(this.result[0]);
                if (_this.result[0]) {
                    _this.message = "Success";
                    console.log("Success");
                    return _this.result;
                }
                else {
                    _this.message = "Error";
                    console.log("Error");
                }
            });
        }
        catch (e) {
            console.log(e);
        }
        ;
    };
    DashboardService.prototype.addStudent = function (username, age) {
        var _this = this;
        try {
            var body = {
                name: username,
                age: age
            };
            this.http.post("http://10.44.72.211:9080/SpringWsAngular/acs/ws/add", body, {}).subscribe(function (data) {
                console.log(data);
                _this.result.push(data[0]);
                _this.result.username = "";
                _this.result.age = "";
                return _this.result;
            });
        }
        catch (e) {
            console.log(e);
        }
        ;
    };
    DashboardService.prototype.backButtonClicked = function () {
        this._location.back();
    };
    return DashboardService;
}());
DashboardService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */]) === "function" && _b || Object])
], DashboardService);

var _a, _b;
//# sourceMappingURL=dashboard.service.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/infant.component.html":
/***/ (function(module, exports) {

module.exports = "<a routerLink=\"/thina\">\r\n    <div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">\r\n        INFANT\r\n    </div>\r\n</a>\r\n<div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">Sent Amount: {{balance}}</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard/infant.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Infant; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_service__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Infant = (function () {
    function Infant(dashboardService, service) {
        this.dashboardService = dashboardService;
        this.service = service;
        this.balance = this.service.getTransferAmount('infant');
    }
    Infant.prototype.ngOnInit = function () {
        this.dashboardService.getStudents();
    };
    return Infant;
}());
Infant = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'infant-dash',
        template: __webpack_require__("../../../../../src/app/dashboard/infant.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */]) === "function" && _b || Object])
], Infant);

var _a, _b;
//# sourceMappingURL=infant.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/swadhin.component.html":
/***/ (function(module, exports) {

module.exports = "<a routerLink=\"/thina\">\r\n    <div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">\r\n        SWADHIN\r\n    </div>\r\n</a>\r\n<div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">Sent Amount: {{balance}}</div>"

/***/ }),

/***/ "../../../../../src/app/dashboard/swadhin.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Swadin; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_service__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Swadin = (function () {
    function Swadin(dashboardService, service) {
        this.dashboardService = dashboardService;
        this.service = service;
        this.balance = this.service.getTransferAmount('swadhin');
    }
    Swadin.prototype.ngOnInit = function () {
        this.dashboardService.getStudents();
    };
    return Swadin;
}());
Swadin = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'swadhin-dash',
        template: __webpack_require__("../../../../../src/app/dashboard/swadhin.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */]) === "function" && _b || Object])
], Swadin);

var _a, _b;
//# sourceMappingURL=swadhin.component.js.map

/***/ }),

/***/ "../../../../../src/app/dashboard/thina.component.html":
/***/ (function(module, exports) {

module.exports = "<a routerLink=\"/thina\">\r\n    <div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">\r\n        THINA\r\n    </div>\r\n</a>\r\n<div class=\"col\" style=\"padding: 2em; margin: 1em 7em; background-color: silver; text-align: center\">Sent Amount: {{balance}}</div>\r\n<input type=\"text\" [(ngModel)]=\"name\"/>\r\n{{name}}"

/***/ }),

/***/ "../../../../../src/app/dashboard/thina.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Thina; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dashboard_service__ = __webpack_require__("../../../../../src/app/dashboard/dashboard.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Thina = (function () {
    function Thina(dashboardService, _location, service) {
        this.dashboardService = dashboardService;
        this._location = _location;
        this.service = service;
        this.balance = this.service.getTransferAmount('thina');
    }
    Thina.prototype.ngOnInit = function () {
        this.dashboardService.getStudents();
        this.name = "<script>alert('DONE');</script>";
    };
    return Thina;
}());
Thina = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'thina-dash',
        template: __webpack_require__("../../../../../src/app/dashboard/thina.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__dashboard_service__["a" /* DashboardService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common__["f" /* Location */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_master_service__["a" /* MasterService */]) === "function" && _c || Object])
], Thina);

var _a, _b, _c;
//# sourceMappingURL=thina.component.js.map

/***/ }),

/***/ "../../../../../src/app/dynamic-form-question/dynamic-form-question.component.html":
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"form\">\n  <label [attr.for]=\"question.key\">{{question.label}}</label>\n\n  <div [ngSwitch]=\"question.controlType\">\n\n    <input *ngSwitchCase=\"'textbox'\" [formControlName]=\"question.key\"\n            [id]=\"question.key\" [type]=\"question.type\">\n\n    <select [id]=\"question.key\" *ngSwitchCase=\"'dropdown'\" [formControlName]=\"question.key\">\n      <option *ngFor=\"let opt of question.options\" [value]=\"opt.key\">{{opt.value}}</option>\n    </select>\n\n  </div> \n\n  <div class=\"errorMessage\" *ngIf=\"!isValid\">{{question.label}} is required</div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dynamic-form-question/dynamic-form-question.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DynamicFormQuestionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_question_base__ = __webpack_require__("../../../../../src/app/modal/question-base.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DynamicFormQuestionComponent = (function () {
    function DynamicFormQuestionComponent() {
    }
    Object.defineProperty(DynamicFormQuestionComponent.prototype, "isValid", {
        get: function () { return this.form.controls[this.question.key].valid; },
        enumerable: true,
        configurable: true
    });
    return DynamicFormQuestionComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__modal_question_base__["a" /* QuestionBase */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__modal_question_base__["a" /* QuestionBase */]) === "function" && _a || Object)
], DynamicFormQuestionComponent.prototype, "question", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */]) === "function" && _b || Object)
], DynamicFormQuestionComponent.prototype, "form", void 0);
DynamicFormQuestionComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-question',
        template: __webpack_require__("../../../../../src/app/dynamic-form-question/dynamic-form-question.component.html")
    })
], DynamicFormQuestionComponent);

var _a, _b;
//# sourceMappingURL=dynamic-form-question.component.js.map

/***/ }),

/***/ "../../../../../src/app/dynamic-form/dynamic-form.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <form (ngSubmit)=\"onSubmit()\" [formGroup]=\"form\">\n\n    <div *ngFor=\"let question of questions\" class=\"form-row\">\n      <app-question [question]=\"question\" [form]=\"form\"></app-question>\n    </div>\n\n    <div class=\"form-row\">\n      <button type=\"submit\" [disabled]=\"!form.valid\">Save</button>\n    </div>\n  </form>\n\n  <div *ngIf=\"payLoad\" class=\"form-row\">\n    <strong>Saved the following values</strong><br>{{payLoad}}\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dynamic-form/dynamic-form.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DynamicFormComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_question_control_service__ = __webpack_require__("../../../../../src/app/services/question-control.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DynamicFormComponent = (function () {
    function DynamicFormComponent(qcs) {
        this.qcs = qcs;
        this.questions = [];
        this.payLoad = '';
    }
    DynamicFormComponent.prototype.ngOnInit = function () {
        this.form = this.qcs.toFormGroup(this.questions);
    };
    DynamicFormComponent.prototype.onSubmit = function () {
        this.payLoad = JSON.stringify(this.form.value);
    };
    return DynamicFormComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* Input */])(),
    __metadata("design:type", Array)
], DynamicFormComponent.prototype, "questions", void 0);
DynamicFormComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-dynamic-form',
        template: __webpack_require__("../../../../../src/app/dynamic-form/dynamic-form.component.html"),
        providers: [__WEBPACK_IMPORTED_MODULE_1__services_question_control_service__["a" /* QuestionControlService */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_question_control_service__["a" /* QuestionControlService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_question_control_service__["a" /* QuestionControlService */]) === "function" && _a || Object])
], DynamicFormComponent);

var _a;
//# sourceMappingURL=dynamic-form.component.js.map

/***/ }),

/***/ "../../../../../src/app/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"text-align: center\">\n  <div class=\"col-md-4 col-lg-4 col-sm-4 col-xs-12\">\n      <input type=\"text\" placeholder=\"Username\" class=\"form-control\" [(ngModel)]=\"username\" />\n      <input type=\"password\" placeholder=\"Password\" class=\"form-control\" [(ngModel)]=\"password\" />\n      <input type=\"submit\" class=\"form-control btn btn-success\" (click)=\"hashedLogin({'username': username, 'password': password})\" />\n      <!--<input type=\"submit\" class=\"form-control btn btn-success\" (click)=\"simpleLogin(username, password)\"/>-->\n      <!--<input type=\"submit\" class=\"form-control btn btn-success\" (click)=\"userLogin(username, password)\"/>-->\n  </div>\n  <br/>\ngsthina\n  <h4>An untrusted URL:</h4>\n  <p><a class=\"e2e-dangerous-url\" [href]=\"dangerousUrl\">Click me</a></p>\n  <h4>A trusted URL:</h4>\n  <p><a class=\"e2e-trusted-url\" [href]=\"trustedUrl\">Click me</a></p>\n</div>"

/***/ }),

/***/ "../../../../../src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_master_service__ = __webpack_require__("../../../../../src/app/services/master.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import * as CryptoJS from '../../../node_modules/crypto-js';
var AES = __webpack_require__("../../../../crypto-js/aes.js");
var SHA256 = __webpack_require__("../../../../crypto-js/sha256.js");
var salt = __webpack_require__("../../../../password-hash-and-salt/index.js");
var passwordHash = __webpack_require__("../../../../password-hash/lib/password-hash.js");
var LoginComponent = (function () {
    function LoginComponent(router, _cacheService, _masterService, http, sanitizer) {
        this.router = router;
        this._cacheService = _cacheService;
        this._masterService = _masterService;
        this.http = http;
        this.sanitizer = sanitizer;
        this.loginMessage = "Login to continue...";
        this.config = {
            baseUrl: "http://10.44.72.211:9080/ProductService/service/"
        };
        // console.log(SHA256("Message"));
        // this.hashedLogin("GSTHINA");
        this.dangerousUrl = 'javascript:alert("Hi there")';
        this.trustedUrl = sanitizer.bypassSecurityTrustUrl(this.dangerousUrl);
    }
    LoginComponent.prototype.ngOnInit = function () {
        this._cacheService.useStorage(__WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["b" /* CacheStoragesEnum */].LOCAL_STORAGE);
        console.log(this._cacheService.exists('current'));
        if (this._cacheService.exists('current')) {
            console.log("Fetched from Cache!");
            this.router.navigate(['/create']);
        }
        else {
            // redirecting to Login
        }
        ;
    };
    LoginComponent.prototype.hashedLogin = function (login) {
        console.log(login);
        // console.log(SHA256(login.password).toString());
        console.log(SHA256(login.password).toString());
        login.password = SHA256(login.password).toString();
        this.userLogin(login);
        // var options = {algorithm:"AES", saltLength:10, iterations: 2}
        // var hashedPassword = passwordHash.generate(password, options);
        // console.log(hashedPassword);
    };
    LoginComponent.prototype.userLogin = function (data) {
        var _this = this;
        var loginData = {
            username: data.username,
            password: data.password.toUpperCase()
        };
        console.log(loginData);
        this.http.post("http://10.44.72.211:9080/ProductService/service/newLogin", loginData).subscribe(function (data) {
            console.log(JSON.stringify(data));
            var res = JSON.parse(JSON.stringify(data));
            if (res.status) {
                _this._cacheService.set('current', res.status);
                _this.router.navigate(['/create']);
            }
            else {
                _this.router.navigate(['/login']);
            }
        });
        // let resp: any = this.doPost("newLogin", loginData).map(data => {
        //   console.log(data);
        // });
        // console.log(resp);
        // .subscribe(data => {
        // if (data) {
        //   console.log(data);
        // } else {
        //   this.loginMessage = "Invalid Credentials";
        // }
        // });
    };
    LoginComponent.prototype.simpleLogin = function (name, pass) {
        if (name === "admin") {
            if (pass === "admin") {
                console.log(this._cacheService.set('current', name));
                window.location.href = "/create";
            }
        }
    };
    LoginComponent.prototype.doPost = function (url, body) {
        var d;
        this.http.post(this.config.baseUrl + url, body).subscribe(function (data) {
            console.log(data);
            d = data;
        });
        return d;
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-login',
        template: __webpack_require__("../../../../../src/app/login/login.component.html"),
        styles: [__webpack_require__("../../../../../src/app/login/login.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__services_master_service__["a" /* MasterService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["b" /* DomSanitizer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["b" /* DomSanitizer */]) === "function" && _e || Object])
], LoginComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ "../../../../../src/app/modal/question-base.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionBase; });
var QuestionBase = (function () {
    function QuestionBase(options) {
        if (options === void 0) { options = {}; }
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.order = options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
    }
    return QuestionBase;
}());

//# sourceMappingURL=question-base.js.map

/***/ }),

/***/ "../../../../../src/app/services/bill.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BillService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BillService = (function () {
    function BillService(_cacheService) {
        this._cacheService = _cacheService;
        this.config = {
            agencyName: "FSS Agency"
        };
    }
    BillService.prototype.deleteBill = function (invoice) {
        console.log("deleteBill :: ", invoice);
        this._cacheService.remove('bill');
    };
    BillService.prototype.saveBill = function (bill) {
        console.log("saveBill :: ", bill);
    };
    BillService.prototype.printBill = function (bill) {
        console.log("printBill :: ", bill);
    };
    BillService.prototype.viewBill = function (bill) {
        console.log("viewBill :: ", bill);
    };
    BillService.prototype.print = function (printContents, bill) {
        var popupWin;
        popupWin = window.open('', '_blank'); //, 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write("\n      <html>\n      <title>" + this.config.agencyName + ' :: ' + bill.invoiceNumber + ("</title>\n      <link rel=\"stylesheet\" href=\"assets/css/style.css\">\n      <link rel=\"stylesheet\" href=\"assets/css/bootstrap.min.css\">\n      <link rel=\"stylesheet\" href=\"assets/css/style.css\">\n      <script src=\"assets/js/jquery.min.js\"></script>\n      <script src=\"assets/js/bootstrap.min.js\"></script>\n        <body onload=\"window.print();window.close()\">\n          <div class=\"row\">\n            <div class=\"originalCopy medium-width inline-block\">" + printContents + "</div>\n            <div class=\"duplicateCopy medium-width right inline-block\">" + printContents + "</div>\n          </div>          \n        </body>\n      </html>"));
        popupWin.document.close();
    };
    return BillService;
}());
BillService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _a || Object])
], BillService);

var _a;
//# sourceMappingURL=bill.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/master.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MasterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MasterService = (function () {
    function MasterService(http) {
        this.http = http;
        this.transferAmount = 0.0;
        this.db = [];
        this.config = {
            baseUrl: "http://10.44.72.211:9080/ProductService/service/"
        };
    }
    MasterService.prototype.setTransferAmount = function (component, amt) {
        var user = {
            component: component,
            amount: amt
        };
        this.db.push(user);
        this.db.forEach(function (element) {
            if (element.component === user.component) {
                element.amount += user.amount;
            }
        });
    };
    MasterService.prototype.getTransferAmount = function (component) {
        this.db.forEach(function (element) {
            if (element.component === component) {
                console.log(element.amount);
                return element.amount;
            }
        });
    };
    MasterService.prototype.doPost = function (url, body) {
        this.http.post(this.config.baseUrl + url, body, {}).subscribe(function (data) {
            console.log(data);
            return data;
        });
    };
    return MasterService;
}());
MasterService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], MasterService);

var _a;
//# sourceMappingURL=master.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/question-control.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuestionControlService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var QuestionControlService = (function () {
    function QuestionControlService() {
    }
    QuestionControlService.prototype.toFormGroup = function (questions) {
        var group = {};
        questions.forEach(function (question) {
            group[question.key] = question.required ? new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */](question.value || '', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["e" /* Validators */].required)
                : new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormControl */](question.value || '');
        });
        return new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormGroup */](group);
    };
    return QuestionControlService;
}());
QuestionControlService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], QuestionControlService);

//# sourceMappingURL=question-control.service.js.map

/***/ }),

/***/ "../../../../../src/app/ws.component.html":
/***/ (function(module, exports) {

module.exports = "<!--<h4><b>{{(result.name)?'Success':'Error'}}</b></h4>-->\r\n<h2 *ngIf=\"_cacheService.exists('current')\">Welcome <b>{{ _cacheService.get('current') }}</b></h2>\r\n<h2 *ngIf=\"!_cacheService.exists('current')\">Log in to continue..</h2>\r\n<div *ngIf=\"_cacheService.exists('current')\">\r\n    <h4 *ngIf=\"result[0].name\" style=\"color: seagreen\"><b>{{message}}</b></h4>\r\n    <h4 *ngIf=\"!result[0].name\" style=\"color: darkred\"><b>{{message}}</b></h4>\r\n    <table class=\"table\" border=\"1\">\r\n        <thead class=\"\">\r\n            <td>User ID</td>\r\n            <td>First Name</td>\r\n            <td>Gender</td>\r\n            <!--<td>Last Name</td>\r\n            <td>Age</td>\r\n            <td>Balance</td>-->\r\n        </thead>\r\n        <tr *ngFor=\"let d of result\">\r\n            <td>{{d.id}}</td>\r\n            <td>{{d.name}}</td>\r\n            <td>{{d.gender}}</td>\r\n            <!--<td>{{d.type}}</td>\r\n            <td>{{d.quantity}}</td>\r\n            <td>{{d.price}}</td>-->\r\n        </tr>\r\n        <tr>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.id\" placeholder=\"ID Number\"/></td>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.name\" placeholder=\"Your Name\" /></td>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.gender\" placeholder=\"Your Gender\"/></td>\r\n            <!--<td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.lname\" /></td>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.age\" /></td>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.balance\" /></td>-->\r\n        </tr>\r\n        <tr>\r\n            <td></td>\r\n            <td>\r\n                <div class=\"btn btn-success\" (click)=\"addUser(result.id, result.name, result.gender, result.password)\">Add Student</div>\r\n                <div class=\"btn btn-danger\" (click)=\"clearCache()\">Clear Cache</div>\r\n            </td>\r\n            <td><input class=\"form-control\" style=\"color: black\" type=\"text\" [(ngModel)]=\"result.password\" placeholder=\"Password\"/></td>\r\n            <!--<td></td>\r\n            <td></td>-->\r\n            \r\n        </tr>\r\n        <!-- add/{name}/{age} -->\r\n    </table>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/ws.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__ = __webpack_require__("../../../../ng2-cache/ng2-cache.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SHA256 = __webpack_require__("../../../../crypto-js/sha256.js");
// declare var BUILD_VERSION: string;
var WsComponent = (function () {
    function WsComponent(http, _cacheService) {
        this.http = http;
        this._cacheService = _cacheService;
        this.result = [{
                name: "",
                age: 0
            }];
        this.message = "Please Wait...";
        console.log("WsComponent :: ", _cacheService.exists('current'));
    }
    WsComponent.prototype.ngOnInit = function () {
        this.fetchData();
    };
    WsComponent.prototype.fetchData = function () {
        var _this = this;
        console.log("Fetched through WS!");
        this.message = "Attempting to fetch through WS";
        // this.http.get("http://10.44.72.211:9080/SpringWsAngular/acs/ws/students").subscribe(data => {
        this.http.get("http://10.44.72.211:9080/ProductService/service/allUsers").subscribe(function (data) {
            // this.result = this.saturateData(data);//.then(saturatedData => {
            _this.result = data;
            console.log("From WS :: ", _this.result);
            if (_this.result[0]) {
                _this.message = "Success";
                _this.message = "WS Working";
                if (_this._cacheService.set('products', _this.result)) {
                    console.log("Updated Cached :: ", _this._cacheService.get('products'));
                }
            }
            else {
                _this.message = "Error";
                console.log("Error");
            }
        });
    };
    WsComponent.prototype.clearCache = function () {
        this._cacheService.removeAll();
        (confirm("Cache Cleared. Reload Now?")) ? this.fetchData() : null;
    };
    WsComponent.prototype.addStudent = function (id, fname, lname, balance, age) {
        var _this = this;
        try {
            var body = { "id": 4321, "name": "ThursDay", "type": "dfgh", "price": 321654, "quantity": 3 };
            // let body = {
            //     id      : id,
            //     name    : fname,
            //     type    : lname,
            //     price   : balance,
            //     quantity: age
            // };
            var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json'
            });
            //continue from here
            // put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response>
            this.http.post("http://10.44.72.211:9080/ProductService/service/save", body, {}).subscribe(function (data) {
                //
                //this.http.get("http://10.44.72.211:9080/SpringWsAngular/acs/ws/add?name=%22Ashok%22&age=25").subscribe(data => {
                // console.log(data);
                console.log(data);
                _this.result.id = "";
                _this.result.fname = "";
                _this.result.lname = "";
                _this.result.age = "";
                _this.result.balance = "";
                // this._cacheService.set('products', this.result);
            });
        }
        catch (e) {
            console.log(e);
        }
        ;
    };
    WsComponent.prototype.addUser = function (id, name, gender, password) {
        var _this = this;
        var body = {
            id: id,
            name: name,
            gender: gender,
            password: password
        };
        body.password = SHA256(body.password).toString();
        console.log(body);
        this.http.post("http://10.44.72.211:9080/ProductService/service/addUser", body, {}).subscribe(function (data) {
            console.log(data);
            console.log(JSON.stringify(data));
            var res = JSON.parse(JSON.stringify(data));
            if (res.status) {
                _this.result.push(body);
                _this.result.id = "";
                _this.result.name = "";
                _this.result.gender = "";
                _this.result.password = "";
                _this.message = "user Added Successfully";
            }
            else {
                _this.message = "Error in Adding User";
            }
        });
    };
    WsComponent.prototype.catch = function (e) {
        console.log(e);
    };
    ;
    return WsComponent;
}());
WsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'ws-component',
        template: __webpack_require__("../../../../../src/app/ws.component.html")
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_cache_ng2_cache__["a" /* CacheService */]) === "function" && _b || Object])
], WsComponent);

var _a, _b;
//# sourceMappingURL=ws.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map